﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Models
{
    [Table("measurementunits")]
    public class UnitOfMeasurements
    {
        [Key]
        public int UnitId { get; set; }
        [Required ]
        [Display(Name ="Unit Name")]
        public string Name { get; set; }
    }
}
