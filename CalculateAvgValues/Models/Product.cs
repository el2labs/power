﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Models
{
    [Table("product")]
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
       
        public string Discribtion { get; set; }
        public int UnitId { get; set; }

        [ForeignKey("UnitId")]
        public UnitOfMeasurements Unit { get; set; }

    }
}
