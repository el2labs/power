﻿using System;

namespace CalculateAvgValues.DataModels
{
    public class Logs
    {
        public int LogID { get; set; }
        public decimal Value { get; set; }
        public DateTime TimeStamp { get; set; }
        
    }
}