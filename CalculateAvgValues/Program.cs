﻿using CalculateAvgValues.Data;
using CalculateAvgValues.DataModels;
using CalculateAvgValues.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateAvgValues
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = GetCircuitsWithLines();

        }
        public static List<CircuitsWithFullAVGDataModel> GetCircuitsWithLines()
        {
            List<CircuitsWithFullAVGDataModel> Circuits = null;

            using(var _context = new DataContext())
            {
                var CircuitsModel = _context.Sources.ToList();
                Circuits=new List<CircuitsWithFullAVGDataModel>();
                foreach(var Circuit in CircuitsModel)
                {
                    Circuits.Add(new CircuitsWithFullAVGDataModel()
                    {
                        CircuitID=Circuit.Id,
                        CircuitName=Circuit.Name,
                        Lines=GetAllLinesForCircuit(Circuit.Id)
                    });
                }
            }
            return Circuits;
        }
        public static List<LinesDataModel> GetAllLinesForCircuit(int id)
        {
            List<LinesDataModel> Lines = null;
            using(var _context = new DataContext())
            {
                var Results = _context.Nodes.Where(n => n.SourceID==id).ToList();
                if(Results!=null)
                {
                    Lines=new List<LinesDataModel>();
                    foreach(var line in Results)
                    {
                        Lines.Add(new LinesDataModel()
                        {
                            LineID=line.NodeID,
                            LineName=line.NodeName,
                            AssginedMeasurments=GetAllAssginedMeasurements(line.NodeID)
                            //
                        });
                    }
                }
            }
            return Lines;
        }

        public static List<MeasurmentsDataModel> GetAllAssginedMeasurements(int LineID)
        {
            List<MeasurmentsDataModel> Results = null;
            using(var _context = new DataContext())
            {
                var Measurments = _context.MeasurmentChart.Where(m => m.NodeID==LineID).ToList();
                if(Measurments.Count()!=0)
                {
                    Results=new List<MeasurmentsDataModel>();
                    foreach(var Item in Measurments)
                    {
                        Results.Add(new MeasurmentsDataModel()
                        {
                            MID=Item.MeasurementID,
                            MName=Item.MeasurementName,
                            LogsData=GetAllLogs(Item.MeasurementID, LineID)
                        });
                    }
                }

            }

            return Results;
        }

        public static List<Logs> GetAllLogs(int MID, int LineID)
        {
            List<Logs> Results = null;
            using(var _context = new DataContext())
            {
                //add time control
                DateTime EndTime = DateTime.Now;
                DateTime startTime = EndTime.AddDays(-1);
                var LogsData = _context.logModel.Where(l => l.NodeID==LineID&&l.MeasurementID==MID&&l.LogTimeStamp>=startTime&&l.LogTimeStamp<=EndTime).ToList();
                if(LogsData.Count()!=0)
                {
                    Results=new List<Logs>();
                    foreach(var log in LogsData)
                    {
                        Results.Add(new Logs()
                        {
                            LogID=log.LogID,
                            Value=log.MeasurementValue,
                            TimeStamp=log.LogTimeStamp
                        });
                    }
                }
            }
            return Results;
        }



    }
}
