﻿using CalculateAvgValues.Models;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Data
{
    public class DataContext : DbContext
    {
  

        public DbSet<Source> Sources { get; set; }
        public DbSet<NodesModel> Nodes { get; set; }
        public DbSet<MeasurmentChartModel> MeasurmentChart { get; set; }
        public DbSet<LogModel> logModel { get; set; }
        public DbSet<SourceDirection> SourceDirections { get; set; }
        public DbSet<MeasuermentTypes> MeasuermentTypes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<UnitOfMeasurements> Units { get; set; }
        public DbSet<Production> Production { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<LinesPowerAVG> AvgPower { get; set; }

    }
}
