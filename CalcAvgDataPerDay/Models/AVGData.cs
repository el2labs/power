﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcAvgDataPerDay.Models
{
    [Table("avgdata")]
   public class AVGData
    {
        [Key]
        public int Id { get; set; }
        public int lineID { get; set; }
        public int cirID { get; set; }
        public decimal voltAVG { get; set; }
        public decimal AmpAVG { get; set; }
        public decimal PFAVG { get; set; }
        public decimal PhaseAVG { get; set; }
        public decimal voltvar { get; set; }
        public decimal Ampvar { get; set; }
        public decimal PFvar { get; set; }
        public decimal Phasevar { get; set; }


        public DateTime Time { get; set; }
        

    }
}
