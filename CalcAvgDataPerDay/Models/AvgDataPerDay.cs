﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcAvgDataPerDay.Models
{
    [Table("avgdataperday")]
   public class AvgDataPerDay
    {
        [Key]
        public int Id { get; set; }
        public int LineID { get; set; }
        public decimal Voltage { get; set; }
        public decimal Ampere { get; set; }
        public decimal PowerFactor { get; set; }
        public decimal Frequancy { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
