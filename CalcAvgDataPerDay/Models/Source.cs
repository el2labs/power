﻿using CalcAvgDataPerDay.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalcAvgData.Models
{
    [Table("source")]
    public class Source
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int SourceCode { get; set; }
        public SourceDirection SourceDirection { get; set; }
        public int SourceDirectionID { get; set; }
        [ForeignKey("SourceID")]
        public IEnumerable<NodesModel> Nodes { get; set; }
        public Location CircuitLocation { get; set; }
        public int LocationID { get; set; }
    }
}
