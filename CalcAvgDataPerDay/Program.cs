﻿using CalcAvgDataPerDay.Data;
using CalcAvgDataPerDay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcAvgDataPerDay
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                DateTime startdate = DateTime.Parse(args[0]);
                DateTime Endate = DateTime.Parse(args[1]);

                var _co = new DataContext();
                var Query = _co.Nodes.Select(n => n.NodeID).ToList();
                foreach (var item in Query)
                {
                    CalculateEach24hForSpasificDay(startdate, Endate, item);
                }

            }
            catch
            {
                var _co = new DataContext();
                var Query = _co.Nodes.Select(n => n.NodeID);
                foreach (var item in Query)
                {
                    CalculateEach24H(item);
                }
            }

        }
        public static void CalculateAvgDataForLine(int lineID , DateTime startDate , DateTime EndDate)
        {
            AvgDataPerDay row = new AvgDataPerDay();
         
            List<decimal> VoltValues = new List<decimal>();
            List<decimal> Ampere = new List<decimal>();
            List<decimal> PowerFactor = new List<decimal>();
            List<decimal> Frequancy = new List<decimal>();
            var _Context = new DataContext();

            var Query = _Context.Avgvalues.Where(a => a.lineID == lineID && a.Time >= startDate && a.Time <= EndDate).ToList();
            if(Query.Any())
            {
                foreach(var item in Query)
                {
                    VoltValues.Add(item.voltAVG);
                    Ampere.Add(item.AmpAVG);
                    PowerFactor.Add(item.PFAVG);
                    Frequancy.Add(item.PhaseAVG);
                }
                row.LineID = lineID;
                row.Voltage = VoltValues.Average();
                var v = VoltValues.Count();
                row.Ampere = Ampere.Average();
                row.PowerFactor = PowerFactor.Average();
                row.Frequancy = Frequancy.Average();
                row.TimeStamp = startDate;


                _Context.AvgDataPerDayValues.Add(row);
                _Context.SaveChanges();
            }
        }
        public static void CalculateEach24hForSpasificDay (DateTime startDate , DateTime endDate , int lineID)
        {
            DateTime tempDate = startDate;
            while(startDate <= endDate)
            {
                tempDate = tempDate.AddDays(1);
                CalculateAvgDataForLine(lineID, startDate, tempDate);
                startDate = tempDate;
            }
        }
        public static void CalculateEach24H(int lineID)
        {
            DateTime EndDate = DateTime.Now;
            DateTime startDate = EndDate.AddDays(-14);

            DateTime tempDate = startDate;
            while (startDate <= EndDate)
            {
                tempDate = tempDate.AddDays(1);
                CalculateAvgDataForLine(lineID, startDate, tempDate);
                startDate = tempDate;
            }
        }


    }
}
