CREATE TABLE `nodes_power_tbl` (
  `NodeID` int(11) NOT NULL,
  `AvgPower` decimal(15,3) NOT NULL,
  `HourPower` datetime NOT NULL,
  KEY `NodeID` (`NodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP PROCEDURE IF EXISTS `calc_power_proc`;


CREATE PROCEDURE `calc_power_proc` ()
BEGIN
   SET @Maxtime =(SELECT SUBDATE(Now(),INTERVAL date_format(NOW(), '%i:%s') MINUTE_SECOND));
   SET @MinTime = (SELECT SUBDATE(@Maxtime, INTERVAL 1 HOUR));

   --  SELECT @Maxtime, @MinTime;

insert into nodes_power_tbl  
   SELECT NodeID, avg(MeasurementValue) AS AvgPower, @MinTime
   FROM logs_tbl
   WHERE     MeasurementID = 5
         AND LogTimeStamp > @MinTime
         AND LogTimeStamp < @Maxtime
   GROUP BY NodeID;
END;

CREATE EVENT `power_calc_evt`
    ON SCHEDULE
      EVERY 1 MINUTE
    DO
      CALL `calc_power_proc`();