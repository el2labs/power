DROP TABLE IF EXISTS `avgdata` ;

CREATE TABLE `avgdata` (
  
`Id` int(11) NOT NULL AUTO_INCREMENT,
  
`lineID` int(11) NOT NULL,
  
`cirID` int(11) DEFAULT NULL,
 
 `voltAVG` decimal(60,2) DEFAULT NULL,
 
 `AmpAVG` decimal(60,2) DEFAULT NULL,
  
`PFAVG` decimal(60,2) DEFAULT NULL,
 
 `PhaseAVG` decimal(60,2) DEFAULT NULL,
 
 `voltvar` decimal(60,2) DEFAULT NULL,
 
 `Ampvar` decimal(60,2) DEFAULT NULL,
  
`PFvar` decimal(60,2) DEFAULT NULL,
  
`Phasevar` decimal(60,2) DEFAULT NULL,
  
`Time` datetime DEFAULT NULL,

 PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1280 DEFAULT CHARSET=utf8;



ALTER TABLE `nodes_power_tbl` 
MODIFY COLUMN `AvgPower` decimal(15, 2) NOT NULL AFTER `NodeID`;


UPDATE measurmenttype
SET `Name` = 'Voltage'
WHERE `Name` = 'Volt';

UPDATE measurmenttype
SET `Name` = 'Power Factor'
WHERE `Name` = 'Phase';