UPDATE measurements_tbl
SET MeasurementName = 'Voltage'
WHERE MeasurementName = 'Volt';

UPDATE measurements_tbl
SET MeasurementName = 'Power Factor'
WHERE MeasurementName = 'Phase';