/*
 Navicat Premium Data Transfer

 Source Server         : Maria
 Source Server Type    : MySQL
 Source Server Version : 100120
 Source Host           : localhost:3312
 Source Schema         : e2l_power

 Target Server Type    : MySQL
 Target Server Version : 100120
 File Encoding         : 65001

 Date: 01/12/2017 23:15:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for logs_tbl
-- ----------------------------
DROP TABLE IF EXISTS `logs_tbl`;
CREATE TABLE `logs_tbl`  (
  `LogID` int(11) NOT NULL AUTO_INCREMENT,
  `MeasurementID` int(11) NULL DEFAULT NULL,
  `MeasurementValue` decimal(10, 3) NULL DEFAULT NULL,
  `LogTimeStamp` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`LogID`) USING BTREE,
  INDEX `MeasurementID`(`MeasurementID`) USING BTREE,
  INDEX `MeasurementID_2`(`MeasurementID`, `LogTimeStamp`) USING BTREE,
  INDEX `LogTimeStamp`(`LogTimeStamp`) USING BTREE,
  INDEX `LogTimeStamp_2`(`LogTimeStamp`, `MeasurementValue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of logs_tbl
-- ----------------------------
INSERT INTO `logs_tbl` VALUES (1, 1, 230.000, '2017-11-30 23:44:51');
INSERT INTO `logs_tbl` VALUES (2, 1, 235.900, NULL);
INSERT INTO `logs_tbl` VALUES (3, 1, 235.900, NULL);
INSERT INTO `logs_tbl` VALUES (4, 1, 235.900, NULL);
INSERT INTO `logs_tbl` VALUES (5, 1, 235.900, NULL);
INSERT INTO `logs_tbl` VALUES (6, 1, 235.900, '2017-12-01 15:51:09');
INSERT INTO `logs_tbl` VALUES (7, 1, 235.900, '2017-12-01 15:51:11');
INSERT INTO `logs_tbl` VALUES (8, 1, 235.900, '2017-12-01 15:51:12');
INSERT INTO `logs_tbl` VALUES (9, 1, 235.900, '2017-12-01 15:51:20');
INSERT INTO `logs_tbl` VALUES (10, 1, 235.900, '2017-12-01 15:51:24');
INSERT INTO `logs_tbl` VALUES (11, 1, 235.900, '2017-12-01 15:51:27');
INSERT INTO `logs_tbl` VALUES (12, 1, 235.900, '2017-12-01 15:51:28');
INSERT INTO `logs_tbl` VALUES (13, 1, 235.900, '2017-12-01 15:51:30');
INSERT INTO `logs_tbl` VALUES (14, 1, 235.900, '2017-12-01 15:51:31');
INSERT INTO `logs_tbl` VALUES (15, 1, 235.900, '2017-12-01 15:51:32');
INSERT INTO `logs_tbl` VALUES (16, 1, 235.900, '2017-12-01 15:51:32');
INSERT INTO `logs_tbl` VALUES (17, 1, 235.900, '2017-12-01 15:51:33');
INSERT INTO `logs_tbl` VALUES (18, 1, 235.900, '2017-12-01 15:51:34');
INSERT INTO `logs_tbl` VALUES (19, 1, 235.900, '2017-12-01 15:51:35');
INSERT INTO `logs_tbl` VALUES (20, 1, 235.900, '2017-12-01 15:51:38');
INSERT INTO `logs_tbl` VALUES (21, 1, 235.900, '2017-12-01 15:51:40');
INSERT INTO `logs_tbl` VALUES (22, 1, 235.900, '2017-12-01 15:52:52');
INSERT INTO `logs_tbl` VALUES (23, 1, 235.900, '2017-12-01 15:53:58');
INSERT INTO `logs_tbl` VALUES (24, 1, 235.900, '2017-12-01 15:53:59');
INSERT INTO `logs_tbl` VALUES (25, 1, 235.900, '2017-12-01 15:54:01');
INSERT INTO `logs_tbl` VALUES (26, 1, 235.900, '2017-12-01 15:54:02');
INSERT INTO `logs_tbl` VALUES (27, 0, 0.000, '2017-12-01 17:30:00');
INSERT INTO `logs_tbl` VALUES (28, 0, 0.000, '2017-12-01 17:30:04');
INSERT INTO `logs_tbl` VALUES (29, 1, 0.000, '2017-12-01 17:30:11');
INSERT INTO `logs_tbl` VALUES (30, 1, 0.000, '2017-12-01 17:30:14');
INSERT INTO `logs_tbl` VALUES (31, 1, 0.000, '2017-12-01 17:30:15');
INSERT INTO `logs_tbl` VALUES (32, 1, 0.000, '2017-12-01 17:30:16');
INSERT INTO `logs_tbl` VALUES (33, 1, 0.000, '2017-12-01 17:30:17');
INSERT INTO `logs_tbl` VALUES (34, 1, 0.000, '2017-12-01 17:30:37');
INSERT INTO `logs_tbl` VALUES (35, 1, 235.900, '2017-12-01 17:30:54');
INSERT INTO `logs_tbl` VALUES (36, 1, 235.900, '2017-12-01 17:30:56');
INSERT INTO `logs_tbl` VALUES (37, 1, 235.900, '2017-12-01 17:30:59');
INSERT INTO `logs_tbl` VALUES (38, 1, 235.900, '2017-12-01 17:31:00');
INSERT INTO `logs_tbl` VALUES (39, 1, 235.900, '2017-12-01 17:31:12');

-- ----------------------------
-- Table structure for measurements_tbl
-- ----------------------------
DROP TABLE IF EXISTS `measurements_tbl`;
CREATE TABLE `measurements_tbl`  (
  `MeasurementID` int(11) NOT NULL AUTO_INCREMENT,
  `MeasurementName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NodeID` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`MeasurementID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of measurements_tbl
-- ----------------------------
INSERT INTO `measurements_tbl` VALUES (1, 'Volt', 1);
INSERT INTO `measurements_tbl` VALUES (2, 'Ampir', 1);
INSERT INTO `measurements_tbl` VALUES (3, 'Phase', 1);

-- ----------------------------
-- Table structure for nodes_tbl
-- ----------------------------
DROP TABLE IF EXISTS `nodes_tbl`;
CREATE TABLE `nodes_tbl`  (
  `NodeID` int(11) NOT NULL AUTO_INCREMENT,
  `NodeName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`NodeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of nodes_tbl
-- ----------------------------
INSERT INTO `nodes_tbl` VALUES (1, 'Power 01');
INSERT INTO `nodes_tbl` VALUES (2, 'Power 02');

SET FOREIGN_KEY_CHECKS = 1;
