/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MariaDB
 Source Server Version : 100211
 Source Host           : localhost:3306
 Source Schema         : e2l_power

 Target Server Type    : MariaDB
 Target Server Version : 100211
 File Encoding         : 65001

 Date: 22/03/2018 12:00:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for logs_tbl
-- ----------------------------
DROP TABLE IF EXISTS `logs_tbl`;
CREATE TABLE `logs_tbl`  (
  `LogID` int(11) NOT NULL AUTO_INCREMENT,
  `MeasurementID` int(11) NULL DEFAULT NULL,
  `NodeID` int(11) NULL DEFAULT NULL,
  `MeasurementValue` decimal(10, 3) NULL DEFAULT NULL,
  `LogTimeStamp` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`LogID`) USING BTREE,
  INDEX `MeasurementID`(`MeasurementID`) USING BTREE,
  INDEX `LogTimeStamp_2`(`LogTimeStamp`, `MeasurementValue`) USING BTREE,
  INDEX `LogTimeStamp`(`LogTimeStamp`) USING BTREE,
  INDEX `MeasurementID_2`(`MeasurementID`, `LogTimeStamp`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for measurements_tbl
-- ----------------------------
DROP TABLE IF EXISTS `measurements_tbl`;
CREATE TABLE `measurements_tbl`  (
  `MeasurementID` int(11) NOT NULL,
  `NodeID` int(11) NOT NULL,
  `MeasurementName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `LineLastValue` decimal(11, 3) NOT NULL,
  PRIMARY KEY (`MeasurementID`, `NodeID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for measurementunits
-- ----------------------------
DROP TABLE IF EXISTS `measurementunits`;
CREATE TABLE `measurementunits`  (
  `UnitId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`UnitId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for measurmenttype
-- ----------------------------
DROP TABLE IF EXISTS `measurmenttype`;
CREATE TABLE `measurmenttype`  (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Unit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for nodes_tbl
-- ----------------------------
DROP TABLE IF EXISTS `nodes_tbl`;
CREATE TABLE `nodes_tbl`  (
  `NodeID` int(11) NOT NULL AUTO_INCREMENT,
  `NodeName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SourceID` int(11) NOT NULL,
  `LineCode` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`NodeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Discribtion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UnitID` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for production
-- ----------------------------
DROP TABLE IF EXISTS `production`;
CREATE TABLE `production`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Time` datetime(0) NULL DEFAULT NULL,
  `ProductId` int(11) NULL DEFAULT NULL,
  `Quantity` decimal(50, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for source
-- ----------------------------
DROP TABLE IF EXISTS `source`;
CREATE TABLE `source`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `SourceCode` int(11) NOT NULL,
  `SourceDirectionID` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for source_direction
-- ----------------------------
DROP TABLE IF EXISTS `source_direction`;
CREATE TABLE `source_direction`  (
  `SourceDirectionID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SourceDirectionID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Triggers structure for table logs_tbl
-- ----------------------------
DROP TRIGGER IF EXISTS `logs_tbl_after_insert`;
delimiter ;;
CREATE TRIGGER `logs_tbl_after_insert` AFTER INSERT ON `logs_tbl` FOR EACH ROW BEGIN
update measurements_tbl set 
measurements_tbl.LineLastValue = NEW.MeasurementValue
WHERE measurements_tbl.MeasurementID = new.MeasurementID AND measurements_tbl.NodeID = New.NodeID;
END
;;
delimiter ;

INSERT INTO `measurmenttype` VALUES (1, 'Volt', 'V');
INSERT INTO `measurmenttype` VALUES (2, 'Ampere', 'A');
INSERT INTO `measurmenttype` VALUES (3, 'Phase', '%');
INSERT INTO `measurmenttype` VALUES (4, 'Frequency', 'HZ');
INSERT INTO `measurmenttype` VALUES (5, 'Power', 'Watt');

SET FOREIGN_KEY_CHECKS = 1;
