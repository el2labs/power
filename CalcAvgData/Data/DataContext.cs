﻿
using CalcAvgData.Models;
using System.Data.Entity;



namespace CalcAvgData.Data
{
    public class DataContext : DbContext
    {


        public DbSet<Source> Sources { get; set; }
        public DbSet<NodesModel> Nodes { get; set; }
        public DbSet<MeasurmentChartModel> MeasurmentChart { get; set; }
        public DbSet<LogModel> logModel { get; set; }
        public DbSet<SourceDirection> SourceDirections { get; set; }
        public DbSet<MeasuermentTypes> MeasuermentTypes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<UnitOfMeasurements> Units { get; set; }
        public DbSet<Production> Production { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<LinesPowerAVG> AvgPower { get; set; }
        public DbSet<AVGData> Avgvalues { get; set; }

    }
}
