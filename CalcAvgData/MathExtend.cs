﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcAvgData
{
    public static class MathExtend
    {
        public static double StandardDeviation(this IEnumerable<double> values)
        {
            if (!values.Any()) return 0;
            double avg = values.Average();
            return Math.Sqrt(values.Average(v => Math.Pow(v-avg, 2)));
        }
    }
}
