﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CalcAvgData.Models
{
    public class MeasurmentLogEntryModel
    {
        [Key]
        public int Id { get; set; }
        public string EntryTime { get; set; }
        public DateTime EntryTimeStamp { get; set; }
        public decimal EntryValue { get; set; }
    }
}
