﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalcAvgData.Models
{
    [Table("nodes_power_tbl")]
    public class LinesPowerAVG
    {
        [Key]
        public int NodeID { get; set; }
        public decimal AvgPower { get; set; }
        public DateTime HourPower { get; set; }
    }
}
