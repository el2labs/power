﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcAvgData.DataModels
{
    public class DataPerHour
    {
        public decimal DataAvg { get; set; }
        public decimal DataVar { get; set; }
    }
}
