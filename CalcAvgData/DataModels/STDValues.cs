﻿using System;

namespace CalculateAvgValues.DataModels
{
    public class STDValues
    {
        public decimal Value { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}