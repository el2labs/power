﻿

using CalcAvgData.Data;
using CalcAvgData.DataModels;
using CalcAvgData.Models;
using CalculateAvgValues.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcAvgData
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DateTime startdate = DateTime.Parse(args[0]);
                DateTime Endate = DateTime.Parse(args[1]);

                var _co = new DataContext();
                var Query = _co.Nodes.Select(n => n.NodeID).ToList();
                foreach (var item in Query)
                {
                    CalcForSpasificDate(item, startdate, Endate);
                }

            }
            catch
            {
                var _co = new DataContext();
                var Query = _co.Nodes.Select(n => n.NodeID).ToList();
                foreach (var item in Query)
                {
                    fun(item);
                }
            }




        }

        public static DataPerHour CalcSTDForHour(DateTime startDate, DateTime EnDate, int MID, int LineID)
        {
            decimal Results = 0;
            DataPerHour Res = new DataPerHour();
            var _Context = new DataContext();
            var LogsData = _Context.logModel.Where(l => l.NodeID == LineID && l.MeasurementID == MID && l.LogTimeStamp >= startDate && l.LogTimeStamp < EnDate).AsNoTracking().AsEnumerable();
            if (LogsData.Any())
            {
                var avgOfHoure = LogsData.Select(o => (double)o.MeasurementValue).Average();
                var max = LogsData.Select(o => (double)o.MeasurementValue).Max();
                var min = LogsData.Select(o => (double)o.MeasurementValue).Min();
                var variance = max - min;
                Results = (decimal)avgOfHoure;
                Res = new DataPerHour() {
                    DataAvg = Results,
                    DataVar = (decimal)variance
                    
                };
            }
            return Res;
        }
        public static int GetCirIDByLineID(int LineID)
        {
            var _co = new DataContext();
            var Query = _co.Nodes.FirstOrDefault(n => n.NodeID == LineID).SourceID;
            return Query;
        }
        public static void fun(int LineID)
        { 
            AVGData Row = null;
            DateTime TempDate;
            DateTime EndDate = DateTime.Now;
            DateTime StartDate = EndDate.AddDays(-1);

            while (StartDate < EndDate)
            {
                TempDate = StartDate.AddHours(1);
                var volt = CalcSTDForHour(StartDate, TempDate, 1, LineID);
                var Amp = CalcSTDForHour(StartDate, TempDate, 2, LineID);
                var pf = CalcSTDForHour(StartDate, TempDate, 3, LineID);
                var freq = CalcSTDForHour(StartDate, TempDate, 4, LineID);
                StartDate = TempDate;

                using (var _context = new DataContext())
                {
                    Row = new AVGData();

                    Row.lineID = LineID;
                    Row.voltAVG = volt.DataAvg;
                    Row.AmpAVG = Amp.DataAvg;
                    Row.PFAVG = pf.DataAvg;
                    Row.PhaseAVG = freq.DataAvg;
                    Row.cirID = GetCirIDByLineID(LineID);
                    Row.voltvar = volt.DataVar;
                    Row.Ampvar = Amp.DataVar;
                    Row.PFvar = pf.DataVar;
                    Row.Phasevar = freq.DataVar;
                    DateTime date = StartDate;
                    Row.Time = date;
                    _context.Avgvalues.Add(Row);
                    _context.SaveChanges();
                }

            }
        }
        public static void CalcForSpasificDate(int LineID, DateTime startDate, DateTime endDate)
        {
            AVGData Row = null;
            var numberOfDays = endDate.Subtract(startDate).Days;

            for (int day = 0; day <= numberOfDays; day++)
            {
                for (int hour = 0; hour < 24; hour++)
                {
                    var timeSlotStart = startDate.AddDays(day).AddHours(hour);
                    var timeSlotEnd = startDate.AddDays(day).AddHours(hour + 1);
                    var volt = CalcSTDForHour(timeSlotStart, timeSlotEnd, 1, LineID);
                    var Amp = CalcSTDForHour(timeSlotStart, timeSlotEnd, 2, LineID);
                    var pf = CalcSTDForHour(timeSlotStart, timeSlotEnd, 3, LineID);
                    var freq = CalcSTDForHour(timeSlotStart, timeSlotEnd, 4, LineID);
                    using (var _context = new DataContext())
                    {
                        Row = new AVGData();

                        Row.lineID = LineID;
                        Row.voltAVG = volt.DataAvg;
                        Row.AmpAVG = Amp.DataAvg;
                        Row.PFAVG = pf.DataAvg;
                        Row.PhaseAVG = freq.DataAvg;
                        Row.cirID = GetCirIDByLineID(LineID);
                        Row.voltvar = volt.DataVar;
                        Row.Ampvar = Amp.DataVar;
                        Row.PFvar = pf.DataVar;
                        Row.Phasevar = freq.DataVar;
                        DateTime date = timeSlotStart;
                        Row.Time = date;
                        _context.Avgvalues.Add(Row);
                        _context.SaveChanges();
                    }
                }
            }
        }
    }

}
