﻿using E2LabPower.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Data
{
    public class DataContext : DbContext
    {
  

        public DbSet<Source> Sources { get; set; }
        public DbSet<NodesModel> Nodes { get; set; }
        public DbSet<MeasurmentChartModel> MeasurmentChart { get; set; }
        public DbSet<LogModel> logModel { get; set; }
        public DbSet<SourceDirection> SourceDirections { get; set; }
        public DbSet<MeasuermentTypes> MeasuermentTypes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<UnitOfMeasurements> Units { get; set; }
        public DbSet<Production> Production { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<LinesPowerAVG> AvgPower { get; set; }
        public DbSet<AVGData> Avgvalues { get; set; }
        public DbSet<AvgDataPerDay> AvgDataPerDayValues { get; set; }
        public DataContext(DbContextOptions<DataContext> op)
        :base(op){}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MeasurmentChartModel>().HasKey(p => new { p.MeasurementID, p.NodeID });
            //modelBuilder.Entity<LogModel>().HasKey(p => new { p.MeasurementID, p.NodeID });
        }
    }
}
