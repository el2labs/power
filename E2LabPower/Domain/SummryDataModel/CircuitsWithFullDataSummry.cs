﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.SummryDataModel
{
    public class CircuitsWithFullDataSummry
    {
        
        public int CircuitID { get; set; }
        public int LineID { get; set; }
        [Display(Name = "Circuit Name")]
        public string CircuitName { get; set; }
        [Display(Name = "Line Name")]
        public string LineName { get; set; }
        [Display(Name = "Voltage average")]
        public decimal VoltAVG { get; set; }
        [Display(Name = "Ampere average")]
        public decimal AmpAVG { get; set; }
        [Display(Name = "Power Factor average")]
        public decimal PFAVG { get; set; }
        [Display(Name = "Frequency average")]
        public decimal PhaseAVG { get; set; }
        [Display(Name = "Voltage Variance")]
        public decimal VoltVar { get; set; }
        [Display(Name = "Ampere Variance")]
        public decimal AmpVar { get; set; }
        [Display(Name = "Power Factor Variance")]
        public decimal PFVAr { get; set; }
        [Display(Name = "Frequency Variance")]
        public decimal PhaseVar { get; set; }

        [Display (Name = "Start Date")]
        [Required(ErrorMessage = "Strat Date Is Required")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        [Required (ErrorMessage ="End Date Is Required")]
        public DateTime EndDate { get; set; }

    }
}
