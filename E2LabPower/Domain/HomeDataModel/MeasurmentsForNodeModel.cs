﻿using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Model
{
    public class MeasurmentsForNodeModel
    {
        public MeasurmentsForNodeModel()
        {
            Measurements = new List<MesurementModel>();
        }
        public List<MesurementModel> Measurements { get; set; }
        public int NodeID { get; set; }
        public int SourceID { get; set; }

    }
}
