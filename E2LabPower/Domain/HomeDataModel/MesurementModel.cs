﻿using System.ComponentModel.DataAnnotations;

namespace E2LabPower.Domain.Model
{
    public class MesurementModel
    {
        public int MeasurmentID { get; set; }
        [Required]
        [Display(Name ="Measurement Units")]
        public string MeasurmentName { get; set; }
        //lineID
        public int ContainerID { get; set; }
    }
}