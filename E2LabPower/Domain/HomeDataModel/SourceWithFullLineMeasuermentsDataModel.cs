﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Model
{
    public class SourceWithFullLineMeasuermentsDataModel
    {
        
        public List<LineModel> Lines { get; set; }

        [Required]
        [Display(Name ="Travel In Hours")]
        public int DateTime { get;  set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
    }
}
