﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Model
{
    public class LineModel
    {
        public int LineID { get; set; }
        public string LineName { get; set; }
        public List<MeasurmentWithDataModel> Measurments { get; set; }
    }
}
