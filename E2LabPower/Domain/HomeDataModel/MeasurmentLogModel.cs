﻿using System;

namespace E2LabPower.Domain.Model
{
    public class MeasurmentLogModel
    {
        public DateTime TimeStamp { get; set; }
        public decimal Value { get; set; }
    }
}