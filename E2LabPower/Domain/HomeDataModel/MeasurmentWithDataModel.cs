﻿using System.Collections.Generic;

namespace E2LabPower.Domain.Model
{
    public class MeasurmentWithDataModel
    {
        public MeasurmentWithDataModel()
        {
            MeasurmentLog = new List<MeasurmentLogModel>();
        }

        public MesurementModel Mesurment { get; set; }
        public List<MeasurmentLogModel> MeasurmentLog { get; set; }
       
    }
}