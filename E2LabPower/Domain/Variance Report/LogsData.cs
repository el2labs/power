﻿using System;

namespace E2LabPower.Domain.Variance_Report
{
    public class LogsData
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public decimal value { get; set; }
    }
}