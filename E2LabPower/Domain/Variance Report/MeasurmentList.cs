﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Variance_Report
{
    public class MeasurmentList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
