﻿using E2LabPower.Domain.AdminSourceDataModel;
using E2LabPower.Domain.DashBordDataModel;
using E2LabPower.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Variance_Report
{
    public class VarianceReportDataModel
    {
        public List<SourceMiniDataModel> SrouceList { get; set; }
        public List<LineModel> ListOFLine { get; set; }
        public List<LogsData> Logs { get; set; }
        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public decimal Avg { get; set; }
        [Required]
        [Display(Name = "circuits")]
        public int SourceID { get; set; }
        [Required]
        [Display(Name = "Lines")]
        public int LineID { get; set; }
        [Required]
        [Display(Name = "Measurements")]
        public int MID { get; set; }
        public List<MeasurmentList> ListOfMeasurment { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        public string LineName { get; set; }
        public string MeasureName { get; set; }

        
    }
}
