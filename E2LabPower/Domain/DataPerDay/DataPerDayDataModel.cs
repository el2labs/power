﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.DataPerDay
{
    public class DataPerDayDataModel
    {
        public int LineID { get; set; }
        [Display(Name = "Line Name")]
        public string LineName { get; set; }
        public int CirID { get; set; }
        [Display(Name = "Circuit Name")]
        public string CirName { get; set; }
        public decimal Voltage { get; set; }
        public decimal Ampere { get; set; }
        public decimal PowerFactor { get; set; }
        public decimal Frequancy { get; set; }
        [Display(Name ="Start Date")]
        public DateTime startDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
    }
}
