﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.ProductionDataModel
{
    public class ProductionEnergyDataModel
    {
        public int ProductionID { get; set; }
        public string ProductionName { get; set; }
        public DateTime SD { get; set; }
        public DateTime ED  { get; set; }
        public List<String> CirCuitsNames { get; set; }
        public decimal  LocationEnergy { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }
}
