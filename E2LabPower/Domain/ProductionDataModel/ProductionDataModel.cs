﻿using E2LabPower.Domain.EnergyWithPro;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.ProductionDataModel
{
    public class ProductionDataModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Start Time")]
        public DateTime Time { get; set; }
        [Required]
        [Display(Name = "End Time")]
        public DateTime EndTime { get; set; }
        [Required]
        [Display(Name = "Products")]
        public int ProductId { get; set; }
        [Required]
        [Display(Name = "Quantity")]
        public decimal Quantity { get; set; }

        public List<ProductData> Products { get; set; }
        public string ProductName { get; set; }
        public decimal ProductionEnergy { get; set; }
        [Required]
        [Display(Name = "Locations")]
        public int LocationID { get; set; }
        public List<LocationDataModel> Locations { get; set; }
        public int CircuitID { get; set; }
        public EnergyWithProDataModel EnergyInfo { get; set; }
        public string UnitOfMeasurment { get; set; }
        public string LocationName { get; set; }
    }
}

