﻿namespace E2LabPower.Domain.ProductionDataModel
{
    public class ProductData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
    }
}