﻿namespace E2LabPower.Domain.AdminSourceDataModel
{
    public class SouceDirectionDataModel
    {
        public int DirectionID { get; set; }
        public string DirectionName { get; set; }
    }
}