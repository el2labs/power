﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.AdminSourceDataModel
{
    public class SourceDataModel
    {
        public int SourceID { get; set; }

        [Required]
        [Display(Name = "Circuit Name")]
        public string SourceName { get; set; }

        [Required]
        [Display(Name = "Circuit Code")]
        public int SourceCode { get; set; }

        [Required]
        [Display(Name = "Circuit Direction")]
        public int DIrectionID { get; set; }


        public string DirectionName { get; set; }

        public int LocationID { get; set; }
        public string LocationName { get; set; }

        public List< SouceDirectionDataModel> Directions { get; set; }

        public List<lineDataModel> Lines { get; set; }
        public List<LocationDataModel> Locations { get; set; }

    }
}
