﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace E2LabPower.Domain.AdminSourceDataModel
{
    public class lineDataModel
    {
        public int LineID { get; set; }
        [Required]
        [Display(Name ="Line Name")]
        public string LineName { get; set; }
        public int SourceID { get; set; }
        [Required]
        [Display(Name = "Line Code")]
        public string LineCode { get; set; }

       
      
    }
}