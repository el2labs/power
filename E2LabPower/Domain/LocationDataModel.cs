﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain
{
    public class LocationDataModel
    {
        public int LocationID { get; set; }
        [Required]
        [Display(Name ="Location Name")]
        public string LocationName { get; set; }
    }
}
