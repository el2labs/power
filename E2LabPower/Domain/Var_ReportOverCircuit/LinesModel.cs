﻿using E2LabPower.Domain.Variance_Report;
using System.Collections.Generic;

namespace E2LabPower.Domain.Var_ReportOverCircuit
{
    public class LinesModel
    {
        public LinesModel()
        {
            Logs=new List<LogsData>();
        }
        public int LineID { get; set; }
        public string LineName { get; set; }
        public List<LogsData> Logs { get; set; }
        public decimal Var { get; set; }
        public decimal AvgForLine { get; set; }
        public decimal EnergyForLine { get; set; }
    }
}