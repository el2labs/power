﻿using E2LabPower.Domain.Variance_Report;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Var_ReportOverCircuit
{
    public class VarReportOverCircuitModel
    {
        [Required]
        [Display(Name ="Circuit")]
        public int SourceID { get; set; }
        [Required]
        [Display(Name ="Measurement")]
        public int MID { get; set; }
        public string SourceName { get; set; }
        public string MName { get; set; }
        public List<MeasurmentList> M_List { set; get; }
        public List<SourceMiniDataModel> Sources { get; set; }
        public List<LinesModel> Lines { get; set; }
        public decimal VarianceResults { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}
