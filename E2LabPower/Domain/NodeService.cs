﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using E2LabPower.Models;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using E2LabPower.Data;
using E2LabPower.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace E2LabPower.Domain
{
    public class LineService
    {
        private IMemoryCache memoryCache;
        private IConfiguration configuration;

        public DataContext datactx { get; }

        public LineService(DataContext Datactx, IMemoryCache MemoryCache, IConfiguration Configurationn)
        {
            datactx = Datactx;
            this.memoryCache = MemoryCache;
            this.configuration = Configurationn;

        }
        internal List<LineModel> GetLinesWithMesurmentFortheLast24hWithSourceID(int sourceID,int DateTime)
        {           
            var result = GetLinesBySourceID(sourceID);
            foreach (var line in result)
            {
                line.Measurments = GetLineWithMesurmentFortheLast24h(line,DateTime);               
            }          
            return result;
        }
        private List<MeasurmentWithDataModel> GetLineWithMesurmentFortheLast24h(LineModel Line,int DateTimee)
        {
            double Hours = DateTimee;
            List<MeasurmentWithDataModel> result = new List<MeasurmentWithDataModel>();
            var query = datactx.MeasurmentChart.Where(o => o.NodeID == Line.LineID).AsNoTracking().ToList();
            foreach (var measurment in query)
            {
                var tempValue = new MeasurmentWithDataModel()
                {
                    Mesurment = new MesurementModel()
                    {
                        MeasurmentID = measurment.MeasurementID,
                        MeasurmentName = measurment.MeasurementName,
                        ContainerID = measurment.NodeID                       
                    }     
            };
                var endDate = DateTime.Now.AddHours(-Hours);
                var logs = datactx.logModel.Where(o => o.MeasurementID == measurment.MeasurementID && o.NodeID == Line.LineID && o.LogTimeStamp >= endDate).AsNoTracking().AsEnumerable();
                foreach (var log in logs)
                {
                    tempValue.MeasurmentLog.Add(new MeasurmentLogModel()
                    {
                        TimeStamp = log.LogTimeStamp,
                        Value = log.MeasurementValue
                    });
                }
                result.Add(tempValue);
            }
            return result;
        }
        public  List<LineModel> GetLinesBySourceID(int sourceID)
        {
            List<LineModel> result = null;
            var query = datactx.Nodes.Where(o => o.SourceID == sourceID).AsNoTracking().AsEnumerable();
            if (query.Any())
            {
                result = new List<LineModel>();
                foreach (var line in query)
                {
                    result.Add(new LineModel()
                    {
                        LineID = line.NodeID,
                        LineName = line.NodeName
                    }
                    );
                }
            }
            return result;
        }
        //Measurment in Renge 
        internal List<LineModel> GetLinesWithMesurmentFortheLast24hWithSourceID_startDate_EndDate(int sourceID, DateTime startTime,DateTime endDate)
        {
            var result = GetLinesBySourceID_startDate_EndDate(sourceID);
            foreach(var line in result)
            {
                line.Measurments=GetLineWithMesurmentFortheLast24h_startDate_EndDate(line, startTime,endDate);
            }
            return result;
        }
        private List<MeasurmentWithDataModel> GetLineWithMesurmentFortheLast24h_startDate_EndDate(LineModel Line, DateTime startTime, DateTime endDate)
        {            
            List<MeasurmentWithDataModel> result = new List<MeasurmentWithDataModel>();
            var query = datactx.MeasurmentChart.Where(o => o.NodeID==Line.LineID).AsNoTracking().AsEnumerable();
            foreach(var measurment in query)
            {
                var tempValue = new MeasurmentWithDataModel()
                {
                    Mesurment=new MesurementModel()
                    {
                        MeasurmentID=measurment.MeasurementID,
                        MeasurmentName=measurment.MeasurementName,
                        ContainerID=measurment.NodeID
                    }
                };             
                var logs = datactx.logModel.Where(o => o.MeasurementID==measurment.MeasurementID&&o.NodeID==Line.LineID&&o.LogTimeStamp >= startTime && o.LogTimeStamp <= endDate).AsNoTracking().AsEnumerable();
                foreach(var log in logs)
                {
                    tempValue.MeasurmentLog.Add(new MeasurmentLogModel()
                    {
                        TimeStamp=log.LogTimeStamp,
                        Value=log.MeasurementValue
                    });
                }
                result.Add(tempValue);
            }
            return result;
        }
        public List<LineModel> GetLinesBySourceID_startDate_EndDate(int sourceID)
        {
            List<LineModel> result = null;

            var query = datactx.Nodes.Where(o => o.SourceID==sourceID).AsNoTracking().AsEnumerable();
            if(query.Any())
            {
                result=new List<LineModel>();
                foreach(var line in query)
                {
                    result.Add(new LineModel()
                    {
                        LineID=line.NodeID,
                        LineName=line.NodeName
                    });
                }
            }
            return result;
        }
    }
}
