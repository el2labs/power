﻿using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain
{
    public class ProductDataModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Product Name")]
        public string Name { get; set; }
        public string MeasuremntUnit { get; set; }
        [Display(Name = "Give Your Product a short Discribtion")]
        public string Discretion { get; set; }
        [Required]
        [Display(Name = "Unit of Measurement")]
        public int UnitId { get; set; }

        public List< UnitOfMeasurements> Units { get; set; }

    }
}
