﻿using E2LabPower.Domain.Var_ReportOverCircuit;
using E2LabPower.Domain.Variance_Report;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.Enargy
{

    public class EnargyDataModel
    {
        public EnargyDataModel()
        {
            this.Lines=new List<LinesModel>();
        }
        [Required]
        [Display(Name ="Circuit")]
        public int SourceID { get; set; }
        public int MID { get; set; }
        public List<SourceMiniDataModel> Sources { get; set; }
        [Required]
        [Display(Name = "Lines")]
        public int LineID { get; set; }
        public List<LinesModel> Lines { get; set; }
        public String LineName { get; set; }
        public string SourceName { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        public decimal EnargyValue { get; set; }
        public decimal EnargyValueKW { get; set; }
        public int NumOfHours { get; set; }
    }
}
