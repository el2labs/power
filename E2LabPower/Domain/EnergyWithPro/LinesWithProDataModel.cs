﻿using System.Collections.Generic;

namespace E2LabPower.Domain.EnergyWithPro
{
    public class LinesWithProDataModel
    {
        public LinesWithProDataModel()
        {
            PowerAVG=new List<LinePowerAVG>();
        }
        public int LineID { get; set; }
        public string LineName { get; set; }
        public List<LinePowerAVG> PowerAVG { get; set; }
        public decimal EnergyPerLineValue { get; set; }
        public decimal EnergyPerLineValueInKW { get; set; }

    }
}