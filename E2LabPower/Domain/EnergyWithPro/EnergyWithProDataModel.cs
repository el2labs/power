﻿using E2LabPower.Domain.Variance_Report;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.EnergyWithPro
{
    public class EnergyWithProDataModel
    {
        public EnergyWithProDataModel()
        {
            Lines=new List<LinesWithProDataModel>();
        }
        [Required]
        [Display(Name = "Circuit")]
        public int CircuitID { get; set; }
        public List<SourceMiniDataModel> Sources { get; set; }
        public List<LinesWithProDataModel> Lines { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        public string CircuitName { get; set; }
        public decimal EnergyPerCirCuitValue { get; set; }
    }
}
