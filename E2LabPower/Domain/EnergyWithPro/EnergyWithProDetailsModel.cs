﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.EnergyWithPro
{
    public class EnergyWithProDetailsModel
    {

        public EnergyWithProDetailsModel()
        {
            EnergyDatalist=new List<EnergyWithProDataModel>();
        }
        public List<EnergyWithProDataModel> EnergyDatalist { get; set; }
        public decimal ProductionEnergy { get; set; }
        public DateTime SD { get; set; }
        public DateTime EndD { get; set; }
        public decimal ProductionQuantity { get; set; }
        public string ProductionName { get; set; }
    }
}
