﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Domain.DashBordDataModel
{
    public class LinesDataModel
    {
        public int LineID { get; set; }
        public string LineName { get; set; }
        public int SourceID { get; set; }

       public List<MeasurmentWithLastValueDataModel> Measuerments { set; get; }
    }
}
