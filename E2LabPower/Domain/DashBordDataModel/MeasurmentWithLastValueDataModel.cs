﻿namespace E2LabPower.Domain.DashBordDataModel
{
    public class MeasurmentWithLastValueDataModel
    {
        public int MeasurementID { get; set; }
        public int MeasurementName { get; set; }
        public string MeasuermentName { get; set; }
        public decimal MeasurementLastValue { get; set; }
        public string MeasurmentUnit { get; set; }
        public int Mini { get; set; }
        public int Max { get; set; }
    }
}