﻿using E2LabPower.Data;
using E2LabPower.Domain.Var_ReportOverCircuit;
using E2LabPower.Domain.Variance_Report;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment.ReportsMangments
{
    public class VarianceReportMangment
    {
        private readonly DataContext _Context;

        public VarianceReportMangment(DataContext context)
        {
            _Context=context;
        }

        //Get All Logs For One Measurment In spasific Time 
        //parmiter: startdate  ,, end date ,, line ID ,, source id ,, measurmentID
        public List<SourceMiniDataModel> GetAllSources()
        {
            List<SourceMiniDataModel> Result = null;
            var Query = _Context.Sources.AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                Result=new List<SourceMiniDataModel>();
                foreach(var S in Query)
                {
                    Result.Add(new SourceMiniDataModel()
                    {
                        SouceID=S.Id,
                        SourceName=S.Name
                    });
                }
            }
            return Result;
        }
        public List<MeasurmentList> GetMeasurmentList(int lineID)
        {
            List<MeasurmentList> result = null;
            var Query = _Context.MeasurmentChart.Where(m => m.NodeID==lineID).ToList();
            if(Query!=null)
            {
                result=new List<MeasurmentList>();
                foreach(var M in Query)
                {
                    result.Add(new MeasurmentList()
                    {
                        Name=M.MeasurementName,
                        Id=GetMeasurmentID(M.MeasurementName)
                    });
                }

            }
            return result;
        }
        internal int GetMeasurmentID(string name)
        {
            int ID = 0;
            var Query = _Context.MeasuermentTypes.SingleOrDefault(t => t.Name==name);
            ID=Query.Id;
            return ID;
        }
        public VarianceReportDataModel CalculateVar(VarianceReportDataModel obj)
        {
            VarianceReportDataModel result = null;
            List<decimal> arr = new List<decimal>();

            var Q = _Context.logModel.Where(p => p.LogTimeStamp>obj.StartDate&&p.LogTimeStamp<obj.EndDate&&p.NodeID==obj.LineID&&p.MeasurementID==obj.MID).AsNoTracking().AsEnumerable();
            result=new VarianceReportDataModel()
            {
                LineID=obj.LineID,
                MID=obj.MID,
                SourceID=obj.SourceID,
                Logs=new List<LogsData>(),
                LineName=new LineMangement(_Context).getLineNameByID(obj.LineID),
                MeasureName=new MeasurementServive(_Context).getM_NameByID(obj.MID)

            };
            foreach(var item in Q)
            {
                arr.Add(item.MeasurementValue);
                result.Logs.Add(new LogsData()
                {
                    Id=item.LogID,
                    Time=item.LogTimeStamp,
                    value=item.MeasurementValue
                });
            }

            if(arr.Any())
            {
                decimal var = arr.Max()-arr.Min();
                result.MaxValue=arr.Max();
                result.MinValue=arr.Min();
                result.Avg=var;
            }
            else
            {
                result.MaxValue=0;
                result.MinValue=0;
                result.Avg=0;
            }

            return result;

        }
        public string  getCircuitname(int id)
        {
            string name = " ";
            var Q = _Context.Sources.SingleOrDefault(s => s.Id==id);
            name=Q.Name;
            return name;
        }
        //calculate Var over Circuit 
        public decimal calculateVarOverLine(List<LogsData> obj)
        {
            decimal LineVar = 0;
            List<decimal> arr = new List<decimal>();
            foreach(var value in obj)
            {
                arr.Add(value.value);
            }
            if(arr.Count()!=0)
            {
                LineVar=arr.Max()-arr.Min();

            }
            return LineVar;
        }
        public List<MeasurmentList> GetAllMeasuremnt()
        {
            List<MeasurmentList> result = null;
            var Query = _Context.MeasuermentTypes.AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                result=new List<MeasurmentList>();
                foreach(var M in Query)
                {
                    result.Add(new MeasurmentList()
                    {
                        Name=M.Name,
                        Id=M.Id
                    });
                }

            }
            return result;
        }
        public List<LinesModel> GetAllLinesByCircuitID(VarReportOverCircuitModel obj)
        {
            List<LinesModel> results = null;
            var Query = _Context.Nodes.Where(l => l.SourceID==obj.SourceID).AsNoTracking().AsEnumerable();
            if(Query!=null)
            {
                results=new List<LinesModel>();
                foreach(var line in Query)
                {
                    results.Add(new LinesModel() { LineID=line.NodeID, LineName=line.NodeName , Logs =GetAllLogsForLineOnSpasificTime(line.NodeID,obj.MID,obj.StartDate,obj.EndDate),
                        Var =calculateVarOverLine(GetAllLogsForLineOnSpasificTime(line.NodeID, obj.MID, obj.StartDate, obj.EndDate))

                    });
                }
            }
            return results;
        }
        public List<LogsData> GetAllLogsForLineOnSpasificTime(int lineID, int MID, DateTime start, DateTime end)
        {
            List<LogsData> Results = null;
            var Q = _Context.logModel.Where(p => p.LogTimeStamp>start&&p.LogTimeStamp<end&&p.NodeID==lineID&&p.MeasurementID==MID).AsNoTracking().AsEnumerable();
            if(Q.Any())
            {
                Results=new List<LogsData>();
                foreach(var M in Q)
                {
                    Results.Add(new LogsData()
                    {
                        Id=M.LogID,
                        Time=M.LogTimeStamp,
                        value=M.MeasurementValue
                    });
                }
            }
            return Results;
        }
        //
        public VarReportOverCircuitModel CalculateVarOverCircuit(VarReportOverCircuitModel obj)
        {
            VarReportOverCircuitModel Result = new VarReportOverCircuitModel()
            {
                SourceID=obj.SourceID,
                MID=obj.MID,
                Lines=GetAllLinesByCircuitID(obj),
                VarianceResults =CalVarOverCir(GetAllLinesByCircuitID(obj)),
                MName =new MeasurementServive(_Context).getM_NameByID(obj.MID),
                SourceName = getCircuitname(obj.SourceID)

            };

            return Result;
        }
        public decimal CalVarOverCir(List<LinesModel> obj)
        {
            decimal finVar;
            List<decimal> arr = new List<decimal>();
            foreach (var line in obj)
            {
                arr.Add(line.Var);
            }
            if(arr.Count()!=0)
            {
                finVar=arr.Max();
            }
            else { finVar=0; }
                return finVar;
        }
        //
        public decimal CalculateVarianceForDetaildData(int LineID, int MID , DateTime nsd, DateTime ned)
        {          
            decimal varResults = 0;
            List<decimal> arr = new List<decimal>();
            var Q = _Context.logModel.Where(p => p.LogTimeStamp>nsd &&p.LogTimeStamp<=ned&&p.NodeID==LineID&&p.MeasurementID==MID).AsNoTracking().AsEnumerable();
            foreach(var item in Q)
            {
                arr.Add(item.MeasurementValue);
            }
            if(arr.Any())
            {
                decimal var = arr.Max()-arr.Min();
                varResults=var;
            }
            else
            { 
                varResults=0;
            }
            return varResults;
        }
    }
}
