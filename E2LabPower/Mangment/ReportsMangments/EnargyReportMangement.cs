﻿using E2LabPower.Data;
using E2LabPower.Domain.Enargy;
using E2LabPower.Domain.Var_ReportOverCircuit;
using E2LabPower.Domain.Variance_Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment.ReportsMangments
{
    public class EnargyReportMangement
    {
        private readonly DataContext _Context;

        public EnargyReportMangement(DataContext context)
        {
            _Context=context;
        }
        public decimal CalculateEnergyPerCircuit(List<LinesModel> lines)
        {
            decimal Result = 0;
            decimal LineEnergies = 0;
            int Counter = 0;
            foreach(var Line in lines)
            {
                LineEnergies=LineEnergies+Line.EnergyForLine;
                Counter++;
            }
            Result=LineEnergies/Counter;
            return Result;
        }
        public List<LinesModel> GetLinesWithEnergy(EnargyDataModel obj)
        {
            List<LinesModel> Lines = null;
            DateTime TempDate;
            DateTime SD = obj.StartDate;
            List<LogsData> TempLogs = new List<LogsData>();
            List<decimal> LogsValue = null;
            List<decimal> LogsAVGValues = new List<decimal>();
            var Query = _Context.Nodes.Where(n => n.SourceID==obj.SourceID).ToList();
            if(Query!=null)
            {
                Lines=new List<LinesModel>();

                foreach(var line in Query)
                {
                    for(double i = 0; SD<obj.EndDate; i++)
                    {
                        SD=SD.Date.AddHours(i);
                        TempDate=SD.Date.AddHours(i+1);
                        var Q = _Context.logModel.Where(p => p.LogTimeStamp>SD&&p.LogTimeStamp<TempDate&&p.NodeID==line.NodeID&&p.MeasurementID==obj.MID).ToList();
                        if(Q.Count!=0)
                        {
                            LogsValue=new List<decimal>();
                            foreach(var log in Q)
                            {
                                TempLogs.Add(new LogsData()
                                {
                                    Id=log.LogID,
                                    Time=log.LogTimeStamp,
                                    value=log.MeasurementValue
                                });
                                LogsValue.Add(log.MeasurementValue);
                            }
                            decimal x = LogsValue.Average();
                            LogsAVGValues.Add(x);
                        }
                    }
                    SD=obj.StartDate;
                    Lines.Add(new LinesModel()
                    {
                        LineID=line.NodeID,
                        LineName=line.NodeName,
                        EnergyForLine=CalculateEnergyKW(LogsAVGValues),
                        Logs=TempLogs
                    });
                    LogsAVGValues=new List<decimal>();
                }
            }
            return Lines;
        }

        public EnargyDataModel CalCulateEnergyForCircuit(EnargyDataModel obj)
        {
            EnargyDataModel Result = new EnargyDataModel()
            {
                SourceID=obj.SourceID,
                MID=5,
                SourceName=new VarianceReportMangment(_Context).getCircuitname(obj.SourceID),
                Lines=GetLinesWithEnergy(obj),
                LineID=obj.LineID,
                LineName=new LineMangement(_Context).getLineNameByID(obj.LineID),
                EnargyValue=CalculateEnergyPerCircuit(GetLinesWithEnergy(obj))


            };
            return Result;
        }
        public List<LinesModel> GetAllLinesByCircuitID(EnargyDataModel obj)
        {
            List<LinesModel> results = null;
            var Query = _Context.Nodes.Where(l => l.SourceID==obj.SourceID).ToList();
            if(Query!=null)
            {
                results=new List<LinesModel>();
                foreach(var line in Query)
                {
                    results.Add(new LinesModel()
                    {
                        LineID=line.NodeID,
                        LineName=line.NodeName,
                        Logs=new VarianceReportMangment(_Context).GetAllLogsForLineOnSpasificTime(line.NodeID, obj.MID, obj.StartDate, obj.EndDate),


                    });
                }
            }
            return results;
        }
        public EnargyDataModel CalculateEnergyFromAVG(EnargyDataModel obj)
        {
            EnargyDataModel energy = null;
            DateTime TempDate;
            DateTime SD = obj.StartDate;
            List<LogsData> TempLogs = new List<LogsData>();
            List<decimal> LogsValue = null;
            List<decimal> LogsAVGValues = new List<decimal>();


            for(double i = 0; SD<obj.EndDate; i++)
            {
                SD=SD.Date.AddHours(i);
                TempDate=SD.Date.AddHours(i+1);

                var Q = _Context.logModel.Where(p => p.LogTimeStamp>SD&&p.LogTimeStamp<TempDate&&p.NodeID==obj.LineID&&p.MeasurementID==obj.MID).ToList();
                if(Q.Count!=0)
                {
                    foreach(var log in Q)
                    {
                        LogsValue=new List<decimal>();
                        TempLogs.Add(new LogsData()
                        {
                            Id=log.LogID,
                            Time=log.LogTimeStamp,
                            value=log.MeasurementValue
                        });

                        LogsValue.Add(log.MeasurementValue);
                    }
                    decimal x = LogsValue.Average();
                    LogsAVGValues.Add(x);

                }
            }
            energy=new EnargyDataModel()
            {
                SourceID=obj.SourceID,
                MID=5,
                SourceName=new VarianceReportMangment(_Context).getCircuitname(obj.SourceID),
                Lines=GetAllLinesByCircuitID(obj),
                EnargyValue=CalculatEnergyFromAVG(LogsAVGValues),
                LineID=obj.LineID,
                LineName=new LineMangement(_Context).getLineNameByID(obj.LineID),
                NumOfHours=LogsAVGValues.Count(),
                EnargyValueKW=CalculateEnergyKW(LogsAVGValues),

            };

            return energy;
        }
        public decimal CalculatEnergyFromAVG(List<decimal> AVGlist)
        {
            decimal EnergyResult = 0;
            decimal Result = 0;
            decimal NoOfHours = 0;
            foreach(var value in AVGlist)
            {
                Result=Result+value;
                NoOfHours++;
            }
            EnergyResult=Result*(NoOfHours/1000);
            return EnergyResult;
        }
        public decimal CalculateEnergyKW(List<decimal> AVGlist)
        {
            decimal EnergyResult = 0;
            decimal Result = 0;
            decimal NoOfHours = 0;
            foreach(var value in AVGlist)
            {
                Result=Result+value;
                NoOfHours++;
            }
            decimal ConstantValue = (decimal)0.001732;
            EnergyResult=Result*NoOfHours*ConstantValue;
            return EnergyResult;
        }

   


    }
}
