﻿using E2LabPower.Data;
using E2LabPower.Domain.EnergyWithPro;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment.ReportsMangments
{
    public class EnergyWithPro
    {
        private readonly DataContext _Context;

        public EnergyWithPro(DataContext context)
        {
            _Context=context;
        }
        //mainFUN lines with full Data
        public List<LinesWithProDataModel> GetAllLinesBySourceID(EnergyWithProDataModel obj)
        {
            List<LinesWithProDataModel> Lines = null;
            var Query = _Context.Nodes.Where(l => l.SourceID==obj.CircuitID).AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                Lines=new List<LinesWithProDataModel>();
                foreach(var line in Query)
                {
                    Lines.Add(new LinesWithProDataModel()
                    {
                        LineID=line.NodeID,
                        LineName=line.NodeName,
                        PowerAVG=GetAVGPowerForLine(line.NodeID, obj.StartDate, obj.EndDate),
                        EnergyPerLineValue=CalculateEnergyForLine(GetAVGPowerForLine(line.NodeID, obj.StartDate, obj.EndDate)),
                        EnergyPerLineValueInKW=CalculateEnergyForLineKW(GetAVGPowerForLine(line.NodeID, obj.StartDate, obj.EndDate))

                    });
                }
            }
            return Lines;
        }
        public List<LinePowerAVG> GetAVGPowerForLine(int LineID, DateTime StartDate, DateTime EndDate)
        {
            List<LinePowerAVG> ListOfPower = new List<LinePowerAVG>();
            DateTime TempDate;
            DateTime SD = StartDate;
            for(double i = 0; SD<EndDate; i++)
            {
                SD=SD.Date.AddHours(i);
                TempDate=SD.Date.AddHours(i+1);
                var AvgPower = _Context.AvgPower.Where(a => a.NodeID==LineID&&a.HourPower>=StartDate&&a.HourPower<=EndDate).AsNoTracking().AsEnumerable();
                if(AvgPower.Any())
                {
                    ListOfPower=new List<LinePowerAVG>();
                    foreach(var Value in AvgPower)
                    {
                        ListOfPower.Add(new LinePowerAVG() { powerAVG=Value.AvgPower });
                    }
                }
            }
            return ListOfPower;
        }
        public decimal CalculateEnergyForLine(List<LinePowerAVG> listOfAVG)
        {
            decimal Results = 0;
            decimal SumOfAVG = 0;
            int Counter = 0;
            if(listOfAVG.Count()!=0)
            {
                foreach(var value in listOfAVG)
                {
                    SumOfAVG+=value.powerAVG;
                    Counter++;
                }
            }
            Results=SumOfAVG*Counter/1000;
            return Results;
        }
        public decimal CalculateEnergyForLineKW(List<LinePowerAVG> listOfAVG)
        {
            decimal Results = 0;
            decimal SumOfAVG = 0;
            int Counter = 0;
            if(listOfAVG.Count()!=0)
            {
                foreach(var value in listOfAVG)
                {
                    SumOfAVG+=value.powerAVG;
                    Counter++;
                }
            }
            double x = 1.732;
           
            decimal factor = Convert.ToDecimal( x/1000);
           
            Results=SumOfAVG*Counter*factor;
            return Results;
        }
        public decimal CalculateEnergyOverCircuit(List<LinesWithProDataModel> Lines)
        {
            decimal Result = 0;
            decimal sumOfEnergy = 0;
            decimal counter = 0;
            foreach(var line in Lines)
            {
                sumOfEnergy+=line.EnergyPerLineValue;
                counter++;
            }
            Result=sumOfEnergy/counter;
            return Result;
        }
        // Mian Fun CircuitWithFull Data
        public EnergyWithProDataModel GetFullData(EnergyWithProDataModel obj)
        {
            var ListOfLines = GetAllLinesBySourceID(obj);
            var ListOfLines2 = GetAllLinesBySourceID(obj);
            if(ListOfLines!=null)
            {
                EnergyWithProDataModel Result = new EnergyWithProDataModel()
                {
                    CircuitID=obj.CircuitID,
                    Lines=GetAllLinesBySourceID(obj),
                    EnergyPerCirCuitValue=CalculateEnergyOverCircuit(ListOfLines),
                    CircuitName=new VarianceReportMangment(_Context).getCircuitname(obj.CircuitID)
                };
                return Result;
            }
            else
            {
                EnergyWithProDataModel Result = new EnergyWithProDataModel()
                {
                    CircuitID=obj.CircuitID,
                    Lines=GetAllLinesBySourceID(obj),
                    EnergyPerCirCuitValue=0,
                    CircuitName=new VarianceReportMangment(_Context).getCircuitname(obj.CircuitID)
                };
                return Result;
            }


        }

        // one Location For N circuits
        public List<EnergyWithProDataModel> CalculateEnergyForLocation(List<EnergyWithProDataModel> EnergyInfoList)
        {
            List<EnergyWithProDataModel> Results = new List<EnergyWithProDataModel>();
            if(EnergyInfoList!=null)
            {

                foreach(var Info in EnergyInfoList)
                {
                    Results.Add(GetFullData(Info));
                }
            }
            return Results;
        }
        public decimal GetEnergyOverLocation(List<EnergyWithProDataModel> ListOfInfo)
        {
            decimal Result = 0;

            foreach(var info in ListOfInfo)
            {
                Result+=info.EnergyPerCirCuitValue;

            }

            return Result;
        }
    }
}
