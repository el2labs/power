﻿using E2LabPower.Data;
using E2LabPower.Domain.Model;
using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class MeasurementServive
    {
        private readonly DataContext _context;
        public MeasurementServive(DataContext context)
        {
            _context = context;
        }
        public List<MeasuermentTypes> GetMeasurmentTypes (DataContext _context)
        {
            var result = _context.MeasuermentTypes.ToList();
            return result;
        }
        public MeasurmentsForNodeModel GetMeasurementForModel (int Id,DataContext con)
        {
            var Query = _context.MeasurmentChart.Where(m => m.NodeID == Id).ToList();
            MeasurmentsForNodeModel result = new MeasurmentsForNodeModel() {
                NodeID = Id,
                SourceID = GetSourceID(Id)
            };           
            foreach(var measuerment in Query)
            {
                var temp = new MesurementModel()
                {
                    MeasurmentID = measuerment.MeasurementID,
                    MeasurmentName = measuerment.MeasurementName,
                    ContainerID = measuerment.NodeID                   
                };
                result.Measurements.Add(temp);
            }
            return result;
        }
        public bool DeleteMeasurement (int NodeID , int MID , DataContext con)
        {
            var model = _context.MeasurmentChart.Single(m => m.MeasurementID == MID && m.NodeID == NodeID);
            if(model != null)
            {
                _context.MeasurmentChart.Remove(model);
                _context.SaveChangesAsync();
                return true;
            }
              return false;
        }
         public int GetSourceID (int LineID)
        {
            int Result = 0;
            var Query = _context.Nodes.Single(l => l.NodeID == LineID);

            Result = Query.SourceID;
            return Result;
        }
        public string getM_NameByID(int id)
        {
            string result = null;
            var Query = _context.MeasuermentTypes.SingleOrDefault(n => n.Id==id);
            if(Query!=null)
            {
                result=Query.Name;
            }
            return result;
        }

    }
}
