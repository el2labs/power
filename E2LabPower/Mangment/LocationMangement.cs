﻿using E2LabPower.Data;
using E2LabPower.Domain;
using E2LabPower.Domain.EnergyWithPro;
using E2LabPower.Domain.Variance_Report;
using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class LocationMangement
    {
        private DataContext _context;
        public LocationMangement(DataContext context)
        {
            _context=context;
        }
#region 
        public List<LocationDataModel> GetAllLocations()
        {
            List<LocationDataModel> LocationList = null;
            var Query = _context.Locations.ToList();
            if(Query!=null)
            {
                LocationList=new List<LocationDataModel>();
                foreach(var location in Query)
                {
                    LocationList.Add(new LocationDataModel()
                    {
                        LocationID=location.LocationID,
                        LocationName=location.LocationName
                    });
                }
            }
            return LocationList;
        }
        public bool CreateNewLocation(LocationDataModel obj)
        {
            if(obj!=null)
            {
                Location DataObj = new Location()
                {
                   
                    LocationName=obj.LocationName
                };
                _context.Locations.Add(DataObj);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public bool ModifyCircuits(int LocationID)
        {
            _context.ChangeTracker.QueryTrackingBehavior=Microsoft.EntityFrameworkCore.QueryTrackingBehavior.NoTracking;
            var CircuitLocation = _context.Sources.Where(s => s.LocationID==LocationID).ToList();
            if(CircuitLocation.Count()!=0)
            {
                foreach(var Circuit in CircuitLocation)
                {
                    Source updated = new Source()
                    {
                        Id=Circuit.Id,
                        Name=Circuit.Name,
                        LocationID=0,
                        SourceDirectionID=Circuit.SourceDirectionID,
                        SourceCode=Circuit.SourceCode
                    };
                    _context.Sources.Update(updated);
                    _context.SaveChangesAsync();
                }
            }
            return true;
        }
        public bool DeleteLocation(int Id)
        {

            var LocationObj = _context.Locations.SingleOrDefault(p => p.LocationID==Id);
            if(LocationObj!=null)
            {
                _context.Locations.Remove(LocationObj);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public bool EditLocation(LocationDataModel obj)
        {
            var location = _context.Locations.Single(l => l.LocationID==obj.LocationID);
            location.LocationName=obj.LocationName;
            _context.Locations.Update(location);
            _context.SaveChangesAsync();

            return true;
        }
        public LocationDataModel GetLocationByID(int Id)
        {
            LocationDataModel location = null;
            var Query = _context.Locations.SingleOrDefault(l => l.LocationID==Id);
            if(Query!=null)
            {
                location=new LocationDataModel()
                {
                    LocationID=Query.LocationID,
                    LocationName=Query.LocationName
                };
            }
            return location;
        }
        public string GetLocationName(int Id)
        {
            string name = " ";
            if(Id!=0)
            {
                var Query = _context.Locations.SingleOrDefault(l => l.LocationID==Id);
                if(Query!=null)
                {
                    name=Query.LocationName;
                }
            }
            return name;
        }
        public int GetCircuitIDByLocationID(int LocationID)
        {
            int CircuitID = 0;
            var Query = _context.Sources.SingleOrDefault(s => s.LocationID==LocationID);

            CircuitID=Query.Id;

            return CircuitID;
        }
        //New Imp N Circuits For One Location 
        public List<EnergyWithProDataModel> GetAllCircuitsWithFullData(int LocationID, DateTime startDate, DateTime enddate)
        {
            List<EnergyWithProDataModel> Result = null;
            var Sources = GetAllCircuitsForLocation(LocationID);
            if(Sources!=null)
            {
                Result=new List<EnergyWithProDataModel>();
                foreach(var circuit in Sources)
                {
                    Result.Add(new EnergyWithProDataModel()
                    {
                        CircuitID=circuit.SouceID,
                        CircuitName=circuit.SourceName,
                        StartDate=startDate,
                        EndDate=enddate
                    });
                }
            }
            return Result;


        }
        public List<SourceMiniDataModel> GetAllCircuitsForLocation(int LocationID)
        {
            List<SourceMiniDataModel> Results = null;
            var Query = _context.Sources.Where(s => s.LocationID==LocationID).ToList();
            if(Query.Count()!=0)
            {
                Results=new List<SourceMiniDataModel>();
                foreach(var Source in Query)
                {
                    Results.Add(new SourceMiniDataModel()
                    {
                        SouceID=Source.Id,
                        SourceName=Source.Name
                    });
                }
            }
            return Results;
        }
        public int GetNoOfCircuitForLocation(int LocationID)
        {
            int numOfCircuits = 0;
            var Query = _context.Sources.Where(c => c.LocationID==LocationID).ToList();
            if(Query.Count()!=0)
            {
                foreach(var Cir in Query)
                {
                    numOfCircuits++;
                }
            }
            return numOfCircuits;
        }
        public int GetLocationIDByProductionID(int ProductionID)
        {
            int LocationID = 0;
            var Query = _context.Production.SingleOrDefault(p => p.Id==ProductionID);
            LocationID=Query.LocationID;
            return LocationID;
        }
#endregion
    }
}
