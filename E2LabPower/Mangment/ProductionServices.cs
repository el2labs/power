﻿using E2LabPower.Data;
using E2LabPower.Domain.EnergyWithPro;
using E2LabPower.Domain.ProductionDataModel;
using E2LabPower.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class ProductionServices
    {
        private DataContext _Context;

        public ProductionServices( DataContext context)
        {
            _Context=context;
        }
        public bool CreateNewProduction(ProductionDataModel obj)
        {
            if(obj!=null)
            {
                Production production = new Production()
                {
                    Time=obj.Time,
                    ProductId=obj.ProductId,
                    Quantity=obj.Quantity,
                    EndTime = obj.EndTime,
                     LocationID = obj.LocationID,
                     ProductionEnergy = obj.ProductionEnergy
                    //calculate Energy

                };

                _Context.Production.Add(production);
                _Context.SaveChangesAsync();

                return true;
            }
            else
            {
                return false;
            }
        }
        public  List<ProductData> GetAllProduct ()
        {
            List<ProductData> Result = null;
            var Query = _Context.Products.AsNoTracking().AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                Result=new List<ProductData>();
                foreach(var Product in Query )
                {
                    Result.Add(new ProductData() {
                        Name = Product.Name,
                        UnitName = new ProductMangement(_Context).GetUnitName(Product.UnitId),
                        UnitID = Product.UnitId,
                        Id = Product.Id
                    });
                }  
            }
            return Result;
        }
        public List<ProductionDataModel> GetAllProduction ()
        {
            List<ProductionDataModel> Result = null;
            var Query = _Context.Production.AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                Result=new List<ProductionDataModel>();
                foreach(var Production  in Query)
                {
                    Result.Add(new ProductionDataModel()
                    {
                        Id=Production.Id,
                        Time=Production.Time,
                        ProductName=GetProductName(Production.ProductId),
                        Quantity=Production.Quantity,
                        EndTime=Production.EndTime,
                        LocationID=Production.LocationID,
                        ProductionEnergy=Production.ProductionEnergy,
                        UnitOfMeasurment=new UnitsMangements(_Context).GetUnitOfMeasurmentByProductID(Production.ProductId),
                        LocationName=new LocationMangement(_Context).GetLocationName(Production.LocationID)
                        
                        //get energy
                        
                    });
                }
            }
            return Result;


        }
        public string GetProductName(int id)
        {
            string name = null;
            var Query = _Context.Products.SingleOrDefault(p => p.Id==id);
            name=Query.Name;
            return name;
        }
        public bool DeleteProduction (int Id)
        {
            var Query = _Context.Production.SingleOrDefault(p => p.Id==Id);
            if(Query != null)
            {
                _Context.Production.Remove(Query);
                _Context.SaveChangesAsync();

                return true;
            }
            return false;
        }
        public ProductionDataModel  GetProductionLogById(int Id)
        {
            ProductionDataModel Result = null;
            var Query = _Context.Production.SingleOrDefault(p => p.Id==Id);
            if(Query != null)
            {               
                Result =new ProductionDataModel() {
                    Id = Query.Id,
                    ProductName = GetProductName(Query.ProductId),
                    Quantity = Query.Quantity,
                    Time = Query.Time,
                    ProductId = Query.ProductId,
                    Products = GetAllProduct(),
                    LocationID = Query.LocationID,
                    EndTime = Query.EndTime,
                    ProductionEnergy = Query.ProductionEnergy,                
                };
                return Result;
            }
            return Result;
        }
        public bool UpdateProductionLog(ProductionDataModel obj)
        {
            if(obj!=null)
            {
                Production QueryVar = new Production()
                {
                    Id=obj.Id,
                    ProductId=obj.ProductId,
                    Quantity=obj.Quantity,
                    Time=obj.Time,
                    EndTime = obj.EndTime,
                    LocationID = obj.LocationID,
                    ProductionEnergy = obj.ProductionEnergy
                    
                };

                _Context.Production.Update(QueryVar);
                _Context.SaveChangesAsync();

                return true;
            }
            return false;
        }
        public EnergyWithProDataModel GetEnergyInfo(int productionID)
        {
            var Query = _Context.Production.SingleOrDefault(p => p.Id==productionID);
            EnergyWithProDataModel Result = new EnergyWithProDataModel() {
               // CircuitID = new LocationMangement(_Context).GetCircuitIDByLocationID(Query.LocationID),
                StartDate = Query.Time,
                EndDate = Query.EndTime,
                EnergyPerCirCuitValue = Query.ProductionEnergy,
                
                
                
                
            };
            return Result;
        }
      
    }
}
