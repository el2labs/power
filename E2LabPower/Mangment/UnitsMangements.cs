﻿using E2LabPower.Data;
using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class UnitsMangements
    {
        private readonly DataContext _Context;

        public UnitsMangements(DataContext DataContext)
        {
            _Context=DataContext;
        }
        public List<UnitOfMeasurements> GetAllUnits()
        {
            var Results = _Context.Units.ToList();

            return Results;
        }
        public bool AddUnit(UnitOfMeasurements obj)
        {
            if(obj!=null)
            {
                _Context.Units.Add(obj);
                _Context.SaveChangesAsync();

                return true;
            }

            return false;
        }
        public bool DeleteUnit(int Id)
        {
            var Query = _Context.Units.SingleOrDefault(u => u.UnitId==Id);
            if(Query!=null)
            {
                _Context.Units.Remove(Query);
                _Context.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public bool CheckTheUnit(string name)
        {
            // false means the name Dose not exist
            bool flag = false;
            var Query = _Context.Units.ToList();
            foreach(var unit in Query)
            {
                if(unit.Name==name)
                {
                    flag=true;
                }

            }
            return flag;
        }
        public bool CheckUnitInProduct(int UnitId)
        {
            var Query = _Context.Products.Where(p => p.UnitId==UnitId).ToList();
            if(Query.Count() == 0)
            {
                return true;
            }
            return false;
        }
        public string GetUnitOfMeasurmentByProductID(int ProductID)
        {
            string name;
            int MeasurmentID;
            var Query = _Context.Products.Single(p => p.Id==ProductID);
            MeasurmentID=Query.UnitId;
            var Query2 = _Context.Units.Single(u => u.UnitId==MeasurmentID);
            name=Query2.Name;
            return name;
        }
    }
}
