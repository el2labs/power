﻿using E2LabPower.Data;
using E2LabPower.Domain.SummryDataModel;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using E2LabPower.Mangment.ReportsMangments;
using Microsoft.EntityFrameworkCore;
using E2LabPower.Domain.DataPerDay;

namespace E2LabPower.Mangment
{
    public class SummryMangments
    {
        private DataContext _Context;
        private readonly IMemoryCache memoryCache;
        private IConfiguration configuration;

        public SummryMangments(DataContext context)
        {
            _Context = context;
        }

        public int GetCID(int lineID)
        {
            int CID = 0;

            var Q = _Context.Nodes.SingleOrDefault(n => n.NodeID == lineID);
            CID = Q.SourceID;
            return CID;
        }

        public List<CircuitsWithFullDataSummry> GetAVGData(DateTime startDate, DateTime endDate)
        {
            List<CircuitsWithFullDataSummry> Results = new List<CircuitsWithFullDataSummry>();
            List<decimal> volt = null;
            List<decimal> amp = null;
            List<decimal> pf = null;
            List<decimal> freq = null;
            List<decimal> voltvar = null;
            List<decimal> amrvar = null;
            List<decimal> pfVar = null;
            List<decimal> freqVar = null;

            var Query = _Context.Nodes.Select(a => a.NodeID);
            foreach (var Line in Query)
            {
                var Query2 = _Context.Avgvalues.Where(a => a.Time >= startDate && a.Time <= endDate && a.lineID == Line).AsNoTracking().AsEnumerable();

                if (Query2.Any())
                {
                    foreach (var item in Query2)
                    {

                        volt = new List<decimal>();
                        amp = new List<decimal>();
                        pf = new List<decimal>();
                        freq = new List<decimal>();
                        voltvar = new List<decimal>();
                        amrvar = new List<decimal>();
                        pfVar = new List<decimal>();
                        freqVar = new List<decimal>();

                        volt.Add(item.voltAVG);
                        amp.Add(item.AmpAVG);
                        pf.Add(item.PFAVG);
                        freq.Add(item.PhaseAVG);
                        voltvar.Add(item.voltvar);
                        amrvar.Add(item.Ampvar);
                        pfVar.Add(item.PFvar);
                        freqVar.Add(item.Phasevar);
                    }

                    Results.Add(new CircuitsWithFullDataSummry()
                    {
                        CircuitID = GetCID(Line),
                        CircuitName = new SourceService(_Context, memoryCache, configuration).GetCircuitNameByID(GetCID(Line)),
                        LineID = Line,
                        LineName = new LineMangement(_Context).getLineNameByID(Line),
                        VoltAVG = volt.Average(),
                        AmpAVG = amp.Average(),
                        PFAVG = pf.Average(),
                        PhaseAVG = freq.Average(),
                        StartDate = startDate,
                        EndDate = endDate,
                        VoltVar = voltvar.Average(),
                        AmpVar = amrvar.Average(),
                        PFVAr = pfVar.Average(),
                        PhaseVar = freqVar.Average()
                    });
                }
            }
            return Results;
        }


        public List<DataPerDayDataModel> GetsumperDay(DateTime startDate, DateTime endDate)
        {
            List<DataPerDayDataModel> Res = new List<DataPerDayDataModel>();
            List<decimal> volt = null;
            List<decimal> amp = null;
            List<decimal> pf = null;
            List<decimal> freq = null;
            var Query = _Context.Nodes.Select(a => a.NodeID);
            foreach (var line in Query)
            {
                var Query2 = _Context.AvgDataPerDayValues.Where(a => a.TimeStamp >= startDate && a.TimeStamp <= endDate && a.LineID == line).AsNoTracking().AsEnumerable();
                if (Query2.Any())
                {



                    foreach (var item in Query2)
                    {
                        volt = new List<decimal>();
                        amp = new List<decimal>();
                        pf = new List<decimal>();
                        freq = new List<decimal>();

                        volt.Add(item.Voltage);
                        amp.Add(item.Ampere);
                        pf.Add(item.PowerFactor);
                        freq.Add(item.Frequancy);


                    }

                    Res.Add(new DataPerDayDataModel()
                    {
                        LineName = new LineMangement(_Context).getLineNameByID(line),
                        CirName = new SourceService(_Context, memoryCache, configuration).GetCircuitNameByID(GetCID(line)),
                        Voltage = volt.Average(),
                        Ampere = amp.Average(),
                        PowerFactor = pf.Average(),
                        Frequancy = freq.Average(),
                    });
                }
            }

            return Res;
        }

    }
}
