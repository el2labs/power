﻿using E2LabPower.Data;
using E2LabPower.Domain;
using E2LabPower.Domain.AdminSourceDataModel;
using E2LabPower.Domain.Model;
using E2LabPower.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{

    public class SourceService
    {
        private DataContext _DbContext;
        public IMemoryCache MemoryCache { get; }
        public IConfiguration Configuration { get; }

        public SourceService(DataContext con, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _DbContext = con;
            MemoryCache = memoryCache;
            Configuration = configuration;
        }
        //Dashboaed
        public IEnumerable<Source> GetAll()
        {
            var Result = _DbContext.Sources.ToList();

            return Result;
        }
        //HomeController
        public SourceWithFullLineMeasuermentsDataModel GetLinesDataOfLast24hForSource(int sourceID , int DateTime )
        {
            SourceWithFullLineMeasuermentsDataModel result=new SourceWithFullLineMeasuermentsDataModel();
            var lineServ = new LineService(_DbContext, MemoryCache, Configuration);
            result.Lines = lineServ.GetLinesWithMesurmentFortheLast24hWithSourceID(sourceID,DateTime);
            return result;
        }
        //measurmentInRange
        public SourceWithFullLineMeasuermentsDataModel GetLinesDataOfLast24hForSourceStartDate(int sourceID, DateTime StartDate, DateTime EndDate)
        {
            SourceWithFullLineMeasuermentsDataModel result = new SourceWithFullLineMeasuermentsDataModel();
            var lineServ = new LineService(_DbContext, MemoryCache, Configuration);
            result.Lines=lineServ.GetLinesWithMesurmentFortheLast24hWithSourceID_startDate_EndDate(sourceID, StartDate, EndDate);
            return result;
        }
        //New Implimentataion
        public List<SourceDataModel> GetAllSources ()
        {
            List<SourceDataModel> Results = null;
            var Query = _DbContext.Sources.AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                Results = new List<SourceDataModel>();
                foreach(var Source in Query)
                {
                    Results.Add(new SourceDataModel() {
                        SourceID = Source.Id,
                        SourceName = Source.Name,
                        SourceCode = Source.SourceCode,
                        DIrectionID = Source.SourceDirectionID,
                        DirectionName = GetDirectionName(Source.SourceDirectionID),
                        Directions = GetAllSourecDirections(),
                        Locations = new LocationMangement(_DbContext).GetAllLocations(),
                        LocationID = Source.LocationID,
                        LocationName = new LocationMangement(_DbContext).GetLocationName(Source.LocationID)
                    });
                }
            }
            return Results;
        }
        public string GetDirectionName (int ID)
        {
            string DName = null;
            var Query = _DbContext.SourceDirections.SingleOrDefault(d => d.SourceDirectionID == ID);
            DName = Query.Name;
            return DName;
        }
        public List< SouceDirectionDataModel> GetAllSourecDirections ()
        {
            List<SouceDirectionDataModel> Directions = null;
            var Query = _DbContext.SourceDirections.AsNoTracking().AsEnumerable();
            if(Query.Any())
            {
                Directions = new List<SouceDirectionDataModel>();
                foreach (var D in Query)
                {
                    Directions.Add(new SouceDirectionDataModel() {

                        DirectionID = D.SourceDirectionID,
                        DirectionName = D.Name
                    });
                }
            }
            return Directions;
        }
        public SourceDataModel GetSourceBYID(int? Id)
        {
            SourceDataModel Result = null;
            var query = _DbContext.Sources.Single(s => s.Id == Id);
            if(query != null)
            {
                Result = new SourceDataModel();
                Result.SourceID = query.Id;
                Result.SourceName = query.Name;
                Result.SourceCode = query.SourceCode;
                Result.DIrectionID = query.SourceDirectionID;
                Result.LocationID=query.LocationID;
                Result.LocationName=new LocationMangement(_DbContext).GetLocationName(query.LocationID);
                
            }


            return Result;

        }
        public bool CreateNewSource(SourceDataModel obj)
        {
            if (obj != null)
            {
                Source NewSource = new Source();
                NewSource.Id = obj.SourceID;
                NewSource.Name = obj.SourceName;
                NewSource.SourceCode = obj.SourceCode;
                NewSource.SourceDirectionID = obj.DIrectionID;
                NewSource.LocationID=obj.LocationID;
                
                _DbContext.Sources.Add(NewSource);
                _DbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> UpdateSourceAsync (SourceDataModel obj)
        {
            if (obj != null)
            {
                Source UpdatedSource = new Source();
                UpdatedSource.Id = obj.SourceID;
                UpdatedSource.Name = obj.SourceName;
                UpdatedSource.SourceCode = obj.SourceCode;
                UpdatedSource.SourceDirectionID = obj.DIrectionID;
                UpdatedSource.LocationID=obj.LocationID;
                _DbContext.Sources.Update(UpdatedSource);
                await _DbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public bool Delete(int SourceID)
        {
            var model = _DbContext.Sources.SingleOrDefault(s => s.Id == SourceID);
            if (model != null)
            {
                _DbContext.Sources.Remove(model);
                _DbContext.SaveChanges();
                return true;
            }
            return false;
        }
        public bool CheckSourceCode(int SourceCode)
        {
                var line = _DbContext.Sources.Where(l => l.SourceCode == SourceCode);
                if (!line.Any())
                {
                    return true;
                }
            return false;
        }
        public string GetCircuitNameByID(int id)
        {
            string name = " ";
            var Query = _DbContext.Sources.Single(s => s.Id==id);
            name=Query.Name;
            return name;
        }
    }
}
