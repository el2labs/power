﻿using E2LabPower.Data;
using E2LabPower.Domain;
using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class ProductMangement
    {
        private readonly DataContext _context;

        public ProductMangement(DataContext context)
        {
            _context=context;
        }
        public List<ProductDataModel> GetAllProduct()
        {
            List<ProductDataModel> Results = null;
            var Query = _context.Products.ToList();
            if(Query!=null)
            {
                Results=new List<ProductDataModel>();
                foreach(var product in Query)
                {
                    Results.Add(new ProductDataModel
                    {

                        Id=product.Id,
                        Name=product.Name,
                        MeasuremntUnit=GetUnitName(product.UnitId),
                        Discretion=product.Discribtion
                    });

                }
            }


            return Results;

        }
        public string GetUnitName(int UnitId)
        {
            string result = " ";
            var Query = _context.Units.SingleOrDefault(u => u.UnitId==UnitId);
            result=Query.Name;
            return result;


        }
        public bool CreateProduct(ProductDataModel obj)
        {
            if(obj!=null)
            {
                Product NewProduct = new Product()
                {

                    Name=obj.Name,

                    Discribtion=obj.Discretion,
                    UnitId=obj.UnitId

                };

                _context.Products.Add(NewProduct);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public bool Delete(int Id)
        {
            var Query = _context.Products.SingleOrDefault(p => p.Id==Id);

            if(Query!=null)
            {
                _context.Products.Remove(Query);
                _context.SaveChangesAsync();

                return true;
            }
            return false;
        }
        public bool Edit(ProductDataModel obj)
        {
            var Query = _context.Products.SingleOrDefault(p => p.Id==obj.Id);

            if(Query!=null)
            {
                Query.Name=obj.Name;

                Query.Discribtion=obj.Discretion;

                _context.Products.Update(Query);
                _context.SaveChangesAsync();
                return true;
            }


            return false;
        }
        public bool UpdateProductData (ProductDataModel obj)
        {
            if(obj != null)
            {
                Product P = new Product();
                P.Id=obj.Id;
                P.Name=obj.Name;
                P.Discribtion=P.Discribtion;
                P.UnitId=obj.UnitId;
                _context.Products.Update(P);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public ProductDataModel GetProductByID(int Id)
        {
            ProductDataModel Result = null;
            var Query = _context.Products.SingleOrDefault(p => p.Id==Id);
            if(Query != null)
            {
                Result=new ProductDataModel() {
                     Id = Query.Id,
                     Name = Query.Name,
                     Discretion = Query.Discribtion,
                     UnitId = Query.UnitId,
                     MeasuremntUnit = GetUnitName(Query.UnitId),
                     Units = new UnitsMangements(_context).GetAllUnits()
                };
            }
            return Result;
        }
    }
}
