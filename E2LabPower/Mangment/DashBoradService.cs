﻿using E2LabPower.Data;
using E2LabPower.Domain.DashBordDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class DashBoradService
    {
        private readonly DataContext _context;

        public DashBoradService(DataContext con)
        {
            _context=con;
        }
        public List<LinesDataModel> GetAllLines(int SourceID)
        {
            List<LinesDataModel> Lines = null;

            var Query = _context.Nodes.Where(L => L.SourceID==SourceID).ToList();
            if(Query!=null)
            {
                Lines=new List<LinesDataModel>();
                foreach(var line in Query)
                {
                    Lines.Add(new LinesDataModel()
                    {

                        LineID=line.NodeID,
                        LineName=line.NodeName,
                        SourceID=line.SourceID



                    });


                }

            }

            return Lines;
        }
        public List<MeasurmentWithLastValueDataModel> GetMeasuermentLastValue(LinesDataModel Line)
        {
            List<MeasurmentWithLastValueDataModel> Results = null;
            var Query = _context.MeasurmentChart.Where(m => m.NodeID==Line.LineID).ToList();
            if(Query!=null)
            {
                Results=new List<MeasurmentWithLastValueDataModel>();

                foreach(var measuerment in Query)
                {
                    Results.Add(new MeasurmentWithLastValueDataModel()
                    {

                        MeasurementID=measuerment.MeasurementID,
                        MeasuermentName=measuerment.MeasurementName,
                        MeasurementLastValue=measuerment.LineLastValue,
                        MeasurmentUnit=GetMeasurementUnit(measuerment.MeasurementID),
                        Max =GetMax(measuerment.MeasurementID)

                    });
                }
            }

            return Results;
        }
        public List<LinesDataModel> GetLinesWithLastValue(int SourceID)
        {
            var Lines = GetAllLines(SourceID);
            foreach(var line in Lines)
            {
                line.Measuerments=GetMeasuermentLastValue(line);

            }

            return Lines;
        }
        public string GetMeasurementUnit(int MeasurmentID)
        {
            string Reslut = " ";
            var Query = _context.MeasuermentTypes.Single(m => m.Id==MeasurmentID);
            Reslut=Query.Unit;
            return Reslut;
        }
        public int GetMax( int MeasurmentID)
        {
            int max = 100;
            var Query = _context.MeasuermentTypes.Single(m => m.Id==MeasurmentID);
            if(Query.Name =="Voltage")
            {
                max=400;
            }else if(Query.Name =="Ampere")
            {
                max=2000;
            }else if (Query.Name =="Power Factor")
            {
                max=1;
            }
            else
            {
                max=100;
            }
            return max;
        }
    }
}
