﻿using E2LabPower.Data;

using E2LabPower.Domain.AdminSourceDataModel;
using E2LabPower.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Mangment
{
    public class LineMangement
    {
        private DataContext _DbContext;

        public LineMangement(DataContext con)
        {
            _DbContext=con;
        }
        public bool CreateNewLine(lineDataModel obj)
        {
            NodesModel NewLine = new NodesModel()
            {
                NodeID=obj.LineID,
                NodeName=obj.LineName,
                SourceID=obj.SourceID,
                LineCode=obj.LineCode
            };
            _DbContext.Nodes.Add(NewLine);
            _DbContext.SaveChangesAsync();

            return true;
        }
        public bool DeleteAllNodesForSource(List<NodesModel> Nodes)
        {
            try
            {
                foreach(var node in Nodes)
                {
                    _DbContext.Nodes.Remove(node);

                }
                _DbContext.SaveChangesAsync();

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return true;
        }
        public List<lineDataModel> GetAllLineForSource(int? id)
        {
            List<lineDataModel> Results = new List<lineDataModel>();
            var query = _DbContext.Nodes.Where(l => l.SourceID==id).AsNoTracking().AsEnumerable();
            if(query.Any())
            {
                Results=new List<lineDataModel>();
                foreach(var line in query)
                {
                    Results.Add(new lineDataModel()
                    {
                        LineID=line.NodeID,
                        LineName=line.NodeName,
                        SourceID=line.SourceID,
                        LineCode=line.LineCode
                    });
                }
            }
            return Results;
        }
        public bool CheckNumberOfLine(int SourceID)
        {
            var lines = GetAllLineForSource(SourceID);
            int NumberOfLines = lines.Count();
            if(NumberOfLines<3)
            {
                return true;
            }




            return false;
        }
        public bool CheckLineCode(string LineCode)
        {


            var line = _DbContext.Nodes.Where(l => l.LineCode==LineCode);
            if(!line.Any())
            {
                return true;
            }


            return false;
        }
        public bool DeleteAllMeasuremnts(int? LineID)
        {

            var Query = _DbContext.MeasurmentChart.Where(m => m.NodeID==LineID).ToList();
            if(Query!=null)
            {
                foreach(var Measuremnt in Query)
                {
                    _DbContext.MeasurmentChart.Remove(Measuremnt);
                    


                }
                _DbContext.SaveChangesAsync();


                return true;
            }



            return false;
        }
        public bool DeleteAllLogs(int? LineID)
        {
            var Query = _DbContext.logModel.Where(l => l.NodeID==LineID).ToList();

            if(Query!=null)
            {
                foreach(var log in Query)
                {
                    _DbContext.logModel.Remove(log);
                }
                _DbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }
        public bool DeleteLine(int? LineID)
        {

            bool MeasurmentsDeleted = DeleteAllMeasuremnts(LineID);
            bool LogsDeleted = DeleteAllLogs(LineID);

            var Query = _DbContext.Nodes.SingleOrDefault(L => L.NodeID==LineID);
            if(Query!=null)
            {
                _DbContext.Nodes.Remove(Query);
                _DbContext.SaveChangesAsync();
                return true;
            }



            return false;
        }
        public int GetCircuitID(int? lineID)
        {
            int Result;
            var Query = _DbContext.Nodes.SingleOrDefault(l => l.NodeID==lineID);

            Result=Query.SourceID;

            return Result;
        }
        public string getLineNameByID(int id)
        {
            string result = null;
            var Query = _DbContext.Nodes.SingleOrDefault(n => n.NodeID==id);
            if(Query != null)
            {
                result=Query.NodeName;
            }
            return result;
        }
        public lineDataModel GetLineByID(int id)
        {
            lineDataModel line = null;
            var lineData = _DbContext.Nodes.Single(l => l.NodeID==id);
            if(lineData !=null)
            {
                line=new lineDataModel();
                line.LineCode=lineData.LineCode;
                line.LineID=lineData.NodeID;
                line.LineName=lineData.NodeName;
               
            }
            return line;
        }
        public async Task<bool> updateAsync (NodesModel nodes)
        {
            NodesModel updated = new NodesModel() {
                NodeID = nodes.NodeID,
                NodeName = nodes.NodeName,
                SourceID = nodes.SourceID,
                LineCode = nodes.LineCode
            };
            _DbContext.Nodes.Update(updated);
            await _DbContext.SaveChangesAsync();
            return true;
        }
    }
}
