﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Primitives;

using E2LabPower.Domain;
using System.IO;
using Microsoft.Extensions.Caching.Memory;
using E2LabPower.Data;
using OfficeOpenXml;
using System.Text;
using E2LabPower.Models;
using Microsoft.EntityFrameworkCore;

namespace E2LabPower.Mangment
{
    [Produces("application/json")]
    public class ExportMangement
    {

        private IConfiguration _Configuration;
        public DataContext Context { get; }
        private IMemoryCache _MemoryCache { get; }

        public ExportMangement(DataContext context)
        {
            Context=context;
        }
        public List<LogModel> GetListToExport(int NodeID, int MID, DateTime sd, DateTime ed)
        {
            List<LogModel> ValuesList = null;
            var values = Context.logModel.Where(l => l.NodeID==NodeID&&l.MeasurementID==MID&&l.LogTimeStamp>=sd&&l.LogTimeStamp<=ed).AsNoTracking().AsEnumerable();
            if(values.Any())
            {
                ValuesList=new List<LogModel>();
                foreach(var row in values)
                {
                    ValuesList.Add(new LogModel()
                    {
                        LogTimeStamp=row.LogTimeStamp,
                        MeasurementValue=row.MeasurementValue

                    });
                }
            }
            return ValuesList;
        }

       
    }
}           
