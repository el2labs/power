﻿using E2LabPower.Domain.DashBordDataModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.ViewComponents
{
    public class DashBoardWithLastValue : ViewComponent
    {
        //highchart for dashboard last value
        public async Task<IViewComponentResult> InvokeAsync(MeasurmentWithLastValueDataModel chartData)
        {
            return View("LastValue", chartData);
        }
    }
}
