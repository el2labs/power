﻿using E2LabPower.Domain.DashBordDataModel;
using E2LabPower.Domain.Model;
using E2LabPower.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.ViewComponents
{
    public class Measurement : ViewComponent
    {
        public IViewComponentResult Invoke(MeasurmentWithDataModel chartData)
        {
            return View(chartData);
        }
        public IViewComponentResult InvokeAsyncLastValue(MeasurmentWithLastValueDataModel chartData)
        {
            return View("LastValue", chartData);
        }
    }
}
