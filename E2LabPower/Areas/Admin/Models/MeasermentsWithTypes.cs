﻿using E2LabPower.Domain.Model;
using E2LabPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Areas.Admin.Models
{
    public class MeasermentsWithTypes
    {
        public MesurementModel Measurement { get; set; }
        public List<MeasuermentTypes> MeasuermentTypes { get; set; }
    }
}
