﻿using E2LabPower.Domain.Model;
using E2LabPower.Domain.Var_ReportOverCircuit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Areas.Admin.ViewComp
{
    public class VarOverCir : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(LinesModel chartData)
        {
            return View("Logs", chartData);
        }
    }
}
