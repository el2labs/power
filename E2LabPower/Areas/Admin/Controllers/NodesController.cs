﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E2LabPower.Data;
using E2LabPower.Models;
using E2LabPower.Domain.AdminSourceDataModel;
using E2LabPower.Mangment;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class NodesController : Controller
    {
        private readonly DataContext _context;

        public NodesController(DataContext context)
        {
            _context=context;
        }
        public async Task<IActionResult> Details(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }

            var nodesModel = await _context.Nodes
                .SingleOrDefaultAsync(m => m.NodeID==id);
            if(nodesModel==null)
            {
                return NotFound();
            }

            return View(nodesModel);
        }
        public IActionResult Create(int SourceID)
        {
            lineDataModel ViewModel = new lineDataModel()
            {
                SourceID=SourceID

            };



            return View(ViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(lineDataModel nodesModel)
        {
            if(ModelState.IsValid)
            {
                bool checkLines = new LineMangement(_context).CheckNumberOfLine(nodesModel.SourceID);
                if(checkLines==true)
                {
                    bool checkLineCode = new LineMangement(_context).CheckLineCode(nodesModel.LineCode);
                    if(checkLineCode)
                    {

                        bool check = new LineMangement(_context).CreateNewLine(nodesModel);
                        return RedirectToAction("Details", "Sources", new { id = nodesModel.SourceID });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The Line Code is Already Exsit");
                        return View(nodesModel);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "You Can NOT Assgine More Than 3 Lines To This Circuit");
                    return View(nodesModel);
                }
            }
            return View(nodesModel);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }

            var nodesModel = await _context.Nodes.SingleOrDefaultAsync(m => m.NodeID==id);
            if(nodesModel==null)
            {
                return NotFound();
            }
            ViewBag.ID=nodesModel.SourceID;
            return View(nodesModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("NodeID,NodeName,SourceID,LineCode")] NodesModel nodesModel)
        {

            if(ModelState.IsValid)
            {
                try
                {

                    bool checkLineCode = true;// new LineMangement(_context).CheckLineCode(nodesModel.LineCode);
                    if(checkLineCode)
                    {

                        _context.Nodes.Update(nodesModel);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        ModelState.AddModelError("", "The Line Code is Already Exsit");
                        return View(nodesModel);
                    }
                }
                catch(DbUpdateConcurrencyException)
                {
                    if(!NodesModelExists(nodesModel.NodeID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Sources", new { id = nodesModel.SourceID });
            }
            return View(nodesModel);
        }
        public IActionResult Delete(int? id)
        {
            int SourceID = new LineMangement(_context).GetCircuitID(id);
            if(id==null)
            {
                return NotFound();
            }
            bool LineDeleted = new LineMangement(_context).DeleteLine(id);
            if(LineDeleted)
            {
                return RedirectToAction("Details", "Sources", new { id = SourceID });
            }
            else
            {
                return NotFound();
            }
        }
        private bool NodesModelExists(int id)
        {
            return _context.Nodes.Any(e => e.NodeID==id);
        }
    }
}
