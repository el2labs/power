﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Domain.ProductionDataModel;
using E2LabPower.Mangment;
using E2LabPower.Data;
using E2LabPower.Domain.EnergyWithPro;
using E2LabPower.Mangment.ReportsMangments;

namespace E2LabPower.Controllers
{
    [Area("Admin")]
    public class ProductionController : Controller
    {
        private DataContext _Context;
        public ProductionController(DataContext context)
        {
            _Context=context;
        }


        #region 
        public IActionResult Index()
        {
            var ViewModel = new ProductionServices(_Context).GetAllProduction();
            return View(ViewModel);
        }
        [HttpGet]
        public IActionResult Create()
        {
            ProductionDataModel ViewMoel = new ProductionDataModel()
            {

                Products=new ProductionServices(_Context).GetAllProduct(),
                Locations=new LocationMangement(_Context).GetAllLocations()
            };

            return View(ViewMoel);
        }
        [HttpPost]
        public IActionResult Create(ProductionDataModel obj)
        {

            var EnergyList = new LocationMangement(_Context).GetAllCircuitsWithFullData(obj.LocationID, obj.Time, obj.EndTime);
            var EnergyListWithData = new EnergyWithPro(_Context).CalculateEnergyForLocation(EnergyList);
            decimal LocationPower = new EnergyWithPro(_Context).GetEnergyOverLocation(EnergyListWithData);
            ProductionDataModel ViewModel = new ProductionDataModel()
            {
                Time=obj.Time,
                EndTime=obj.EndTime,
                Quantity=obj.Quantity,
                LocationID=obj.LocationID,
                ProductId=obj.ProductId,
                Products=new ProductionServices(_Context).GetAllProduct(),
                Locations=new LocationMangement(_Context).GetAllLocations()

            };


            if(ModelState.IsValid)
            {
                obj.LocationName=new LocationMangement(_Context).GetLocationName(obj.LocationID);
                obj.ProductionEnergy=LocationPower/obj.Quantity;

                bool Created = new ProductionServices(_Context).CreateNewProduction(obj);
                if(Created)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View("Create", ViewModel);
            }


        }
        public IActionResult Delete(int Id)
        {

            bool Deleted = new ProductionServices(_Context).DeleteProduction(Id);
            return RedirectToAction(nameof(Index));

        }
        [HttpGet]
        public IActionResult Edit(int Id)
        {
            var x = new ProductionServices(_Context).GetProductionLogById(Id);
            ProductionDataModel ViewModel = new ProductionDataModel()
            {
                Id=x.Id,
                ProductId=x.ProductId,
                EndTime=x.EndTime,
                Time=x.Time,
                LocationID=x.LocationID,
                Products=new ProductionServices(_Context).GetAllProduct(),
                Locations=new LocationMangement(_Context).GetAllLocations(),
                Quantity = x.Quantity
            };
            return View(ViewModel);
        }
        [HttpPost]
        public IActionResult Edit(ProductionDataModel obj)
        {

            var EnergyList = new LocationMangement(_Context).GetAllCircuitsWithFullData(obj.LocationID, obj.Time, obj.EndTime);
            var EnergyListWithData = new EnergyWithPro(_Context).CalculateEnergyForLocation(EnergyList);
            decimal LocationPower = new EnergyWithPro(_Context).GetEnergyOverLocation(EnergyListWithData);


            if(ModelState.IsValid)
            {
                obj.LocationName=new LocationMangement(_Context).GetLocationName(obj.LocationID);
                if(obj.Quantity!=0)
                {
                    obj.ProductionEnergy=LocationPower/obj.Quantity;

                }
                else
                {
                    obj.ProductionEnergy=LocationPower;
                }
                bool updated = new ProductionServices(_Context).UpdateProductionLog(obj);
                if(updated)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                obj.Products=new ProductionServices(_Context).GetAllProduct();
                obj.Locations=new LocationMangement(_Context).GetAllLocations();
                return View("Edit", obj);
            }
        }
        public IActionResult EnergyDetails(int Id)
        {

            int LocationID = new LocationMangement(_Context).GetLocationIDByProductionID(Id);
            var productionInfo = new ProductionServices(_Context).GetProductionLogById(Id);
            var EnergyInfo = new ProductionServices(_Context).GetEnergyInfo(Id);
            var EnergyList = new LocationMangement(_Context).GetAllCircuitsWithFullData(LocationID, EnergyInfo.StartDate, EnergyInfo.EndDate);
            var EnergyListWithData = new EnergyWithPro(_Context).CalculateEnergyForLocation(EnergyList);
            decimal LocationPower = new EnergyWithPro(_Context).GetEnergyOverLocation(EnergyListWithData);

            EnergyWithProDetailsModel ViewModel = new EnergyWithProDetailsModel()
            {
                ProductionEnergy=productionInfo.ProductionEnergy,
                SD = productionInfo.Time,
                EndD = productionInfo.EndTime,
                ProductionQuantity = productionInfo.Quantity,
                ProductionName = productionInfo.ProductName
            };
            foreach(var Item in EnergyListWithData)
            {
                ViewModel.EnergyDatalist.Add(new EnergyWithProDataModel()
                {
                    CircuitID=Item.CircuitID,
                    CircuitName=Item.CircuitName,
                    EnergyPerCirCuitValue=Item.EnergyPerCirCuitValue
                    

                });
            }



            return View(ViewModel);
        }



        #endregion
    }
}