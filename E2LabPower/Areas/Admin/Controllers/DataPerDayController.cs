﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Data;
using E2LabPower.Mangment;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("admin")]
    public class DataPerDayController : Controller
    {
        private DataContext _context;

        public DataPerDayController(DataContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult GetDataPerDay()
        {
            return View();
        }
        [HttpPost]
        public IActionResult DetaildDataperDay(DateTime startDate, DateTime EndDate)
        {
            if (ModelState.IsValid)
            {
                  var data = new SummryMangments(_context).GetsumperDay(startDate, EndDate);
                ViewData["sd"] = startDate;
                ViewData["ed"] = EndDate;
                return View(data);
            }
            else
            {
                return View("GetDataPerDay");
            }
        }

    }
}