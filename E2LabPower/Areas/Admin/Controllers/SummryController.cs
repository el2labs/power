﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Mangment;
using E2LabPower.Data;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SummryController : Controller
    {
        private DataContext _context;

        public SummryController(DataContext context)
        {
            _context=context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult GetSummry()
        {
            return View();
        }
        [HttpPost]
        public IActionResult DetaildData(DateTime TimeDate, DateTime EndDate)
        {
            if(ModelState.IsValid)
            {
                var data = new SummryMangments(_context).GetAVGData(TimeDate, EndDate);
                ViewData["sd"]=TimeDate;
                ViewData["ed"]=EndDate;
                return View(data);
            }
            else
            {
                return View("GetSummry");
            }
        }

    }
}