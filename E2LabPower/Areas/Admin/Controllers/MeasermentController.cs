﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Areas.Admin.Models;
using E2LabPower.Data;
using E2LabPower.Mangment;
using E2LabPower.Domain.Model;
using E2LabPower.Models;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class MeasermentController : Controller
    {
        private readonly DataContext _context;


        public MeasermentController(DataContext context)
        {
            _context=context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Create(int Id)
        {
            ViewData["NodeID"]=Id;
            MeasurementServive servive = new MeasurementServive(_context);
            var viewModel = new MeasermentsWithTypes()
            {
                MeasuermentTypes=servive.GetMeasurmentTypes(_context)
            };
            return View(viewModel);
        }
        [HttpPost]
        public IActionResult Create(int id, MeasermentsWithTypes obj)
        {
            if(ModelState.IsValid)
            {
                var temp = _context.MeasuermentTypes.SingleOrDefault(m => m.Name==obj.Measurement.MeasurmentName);
                var NewMeasuerment = new MeasurmentChartModel()
                {
                    NodeID=id,
                    MeasurementID=temp.Id,
                    MeasurementName=obj.Measurement.MeasurmentName
                };
                _context.MeasurmentChart.Add(NewMeasuerment);
                _context.SaveChangesAsync();
                return RedirectToAction("MeasurementDetails", "Measerment", new { id = id });
            }
            else
            {
                MeasurementServive servive = new MeasurementServive(_context);
                var viewModel = new MeasermentsWithTypes()
                {
                    MeasuermentTypes=servive.GetMeasurmentTypes(_context)
                };
                return View(viewModel);
            }
        }
        public IActionResult MeasurementDetails(int Id)
        {
            MeasurementServive servive = new MeasurementServive(_context);
            var viewModel = servive.GetMeasurementForModel(Id, _context);
            return View(viewModel);
        }
        public IActionResult DeleteMeasurement(int NodeID, int MID)
        {
            bool result = false;
            MeasurementServive service = new MeasurementServive(_context);
            result=service.DeleteMeasurement(NodeID, MID, _context);
            return RedirectToAction("MeasurementDetails", "Measerment", new { id = NodeID });
        }


    }




}