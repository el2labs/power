﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Domain.Enargy;
using E2LabPower.Mangment.ReportsMangments;
using E2LabPower.Data;
using E2LabPower.Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class EnargyController : Controller
    {
        private readonly DataContext _Context;
        private IMemoryCache memoryCache;
        private IConfiguration configuration;

        public EnargyController(DataContext data)
        {
            _Context=data;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult CalCulateEnargy()
        {
            var ViewModel = new EnargyDataModel()
            {
                Sources=new VarianceReportMangment(_Context).GetAllSources(),
            };

            return View(ViewModel);

        }
        [HttpGet]
        public JsonResult GetLines(int SourceID)
        {

            var Data = new LineService(_Context, memoryCache, configuration).GetLinesBySourceID(SourceID).ToList();
            return Json(new SelectList(Data, "LineID", "LineName"));
        }
        [HttpPost]
        public IActionResult GetEnargy(EnargyDataModel obj)
        {
         
            if(ModelState.IsValid)
            {
                obj.MID=5;
                var z = new EnargyReportMangement(_Context).CalculateEnergyFromAVG(obj);
                if(z.EnargyValue!=0)
                {
                    return View(z);
                }
                else
                {
                    ModelState.AddModelError("", "There Is No Energy In this Circuit");
                    return View(z);
                }
            }
            else
            {
                obj.Sources=new VarianceReportMangment(_Context).GetAllSources();
                return View("CalCulateEnargy", obj);
            }
        }
        public IActionResult CalculateEnergyPerCircuit ()
        {
            var ViewModel = new EnargyDataModel()
            {
                Sources=new VarianceReportMangment(_Context).GetAllSources(),
            };
            return View(ViewModel);
        }
        public IActionResult GetEnergyForCircuit(EnargyDataModel obj)
        {
            if(ModelState.IsValid)
            {
                obj.MID=5;
                var ViewModel = new EnargyReportMangement(_Context).CalCulateEnergyForCircuit(obj);
                if(ViewModel.EnargyValue!=0)
                {
                    return View(ViewModel);
                }
                else
                {
                    ModelState.AddModelError("", "There Is No Energy In this Circuit");
                    return View(ViewModel);
                }
            }
            else
            {
                obj.Sources=new VarianceReportMangment(_Context).GetAllSources();
                return View("CalCulateEnargy", obj);
            }
           
            
           
        }
    }
}