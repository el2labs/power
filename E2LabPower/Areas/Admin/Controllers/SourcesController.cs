﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E2LabPower.Data;
using E2LabPower.Models;
using E2LabPower.Mangment;
using E2LabPower.Areas.Admin.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using E2LabPower.Domain.AdminSourceDataModel;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SourcesController : Controller
    {
        private readonly DataContext _context;
        private IMemoryCache memoryCache { get; }
        private IConfiguration configurationn { get; }

        public SourcesController(DataContext context, IMemoryCache MemoryCache, IConfiguration Configurationn)
        {
            _context=context;
            this.memoryCache=MemoryCache;
            this.configurationn=Configurationn;
        }
        //Actions
        //Index
        public IActionResult Index()
        {
            var Model = new SourceService(_context, memoryCache, configurationn).GetAllSources();
            return View(Model);
        }
        public IActionResult CreateNewSource()
        {
            SourceDataModel Model = new SourceDataModel()
            {
                Directions=new SourceService(_context, memoryCache, configurationn).GetAllSourecDirections(),
                Locations=new LocationMangement(_context).GetAllLocations()
            };
            return View(Model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("SourceID,SourceName,SourceCode,DIrectionID,LocationID")] SourceDataModel source)
        {
            var viewmodel = new SourceDataModel
            {
                DIrectionID=source.DIrectionID,
                SourceCode=source.SourceCode,
                SourceName=source.SourceName,
                Directions=new SourceService(_context, memoryCache, configurationn).GetAllSourecDirections(),
                LocationID=source.LocationID,
                Locations=new LocationMangement(_context).GetAllLocations()
            };
            if(ModelState.IsValid)
            {
                bool checkSourceCode = new SourceService(_context, memoryCache, configurationn).CheckSourceCode(source.SourceCode);
                if(checkSourceCode)
                {
                    bool Result = new SourceService(_context, memoryCache, configurationn).CreateNewSource(source);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ModelState.AddModelError("", "The Line Code is Already Exsit");
                    return View("CreateNewSource", viewmodel);
                }
            }
            return View("CreateNewSource", viewmodel);
        }
        public IActionResult Details(int? id)
        {
            SourceDataModel viewModel = new SourceDataModel();
            if(id==null)
            {
                return NotFound();
            }    
            var source = new SourceService(_context, memoryCache, configurationn).GetSourceBYID(id);
            if(source==null)
            {
                return NotFound();
            }
            viewModel.SourceID=source.SourceID;
            viewModel.SourceName=source.SourceName;
            viewModel.SourceCode=source.SourceCode;
            viewModel.DIrectionID=source.DIrectionID;
            viewModel.Lines=new LineMangement(_context).GetAllLineForSource(id);
            return View(viewModel);
        }
        public IActionResult Edit(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }
            var source = new SourceService(_context, memoryCache, configurationn).GetSourceBYID(id);
            if(source==null)
            {
                return NotFound();
            }
            var ViewModel = new SourceDataModel()
            {
                SourceName=source.SourceName,
                SourceCode=source.SourceCode,
                DIrectionID=source.DIrectionID,
                SourceID=source.SourceID,
                Directions=new SourceService(_context, memoryCache, configurationn).GetAllSourecDirections(),
                Locations = new LocationMangement(_context).GetAllLocations()
            };
            return View(ViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SourceID,SourceName,SourceCode,DIrectionID,LocationID", "Directions,Locations")] SourceDataModel source)
        {
            source.Directions=new SourceService(_context, memoryCache, configurationn).GetAllSourecDirections();
            source.Locations=new LocationMangement(_context).GetAllLocations();
            if(id!=source.SourceID)
            {
                return NotFound();
            }
            if(ModelState.IsValid)
            {
                try
                {
                    bool check = await new SourceService(_context, memoryCache, configurationn).UpdateSourceAsync(source);
                }
                catch(DbUpdateConcurrencyException)
                {
                    if(!SourceExists(source.SourceID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(source);
        }
        public IActionResult Delete(int id)
        {
            var SourceMange = new SourceService(_context, memoryCache, configurationn);
            var nodemange = new LineMangement(_context);
            if(id==null)
            {
                return NotFound();
            }
            var Nodes = _context.Nodes.Where(n => n.SourceID==id).ToList();
            if(SourceMange.Delete(id)&&nodemange.DeleteAllNodesForSource(Nodes))
            {
                return RedirectToAction("Index", "Sources");
            }
            else
            {
                return NotFound();
            }
        }
        private bool SourceExists(int id)
        {
            return _context.Sources.Any(e => e.Id==id);
        }
    }
}
