﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Domain.Variance_Report;
using E2LabPower.Mangment.ReportsMangments;
using E2LabPower.Data;
using Microsoft.AspNetCore.NodeServices;
using E2LabPower.Domain;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using E2LabPower.Domain.Model;
using Microsoft.AspNetCore.Mvc.Rendering;
using E2LabPower.Mangment;
using E2LabPower.Domain.Var_ReportOverCircuit;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class VarianceController : Controller
    {
        private readonly DataContext _Context;
        private IMemoryCache memoryCache;
        private IConfiguration configuration;
        VarianceReportDataModel FinalObj = new VarianceReportDataModel();
        public VarianceController(DataContext context)
        {
            _Context=context;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult CalVar()
        {

            VarianceReportDataModel viewmodel = new VarianceReportDataModel()
            {
                SrouceList=new VarianceReportMangment(_Context).GetAllSources(),
            };

            return View(viewmodel);
        }
        [HttpGet]
        public JsonResult GetLines(int SourceID)
        {

            var Data = new LineService(_Context, memoryCache, configuration).GetLinesBySourceID(SourceID).ToList();
            return Json(new SelectList(Data, "LineID", "LineName"));
        }
        [HttpGet]
        public JsonResult GetLineMeasurments(int LineID)
        {

            var Data = new VarianceReportMangment(_Context).GetMeasurmentList(LineID);
            return Json(new SelectList(Data, "Id", "Name"));
        }
        [HttpPost]
        public IActionResult CalVariance(VarianceReportDataModel obj)
        {
            if(ModelState.IsValid)
            {
                var x = new VarianceReportMangment(_Context).CalculateVar(obj);
                if(x==null)
                {
                    return NotFound();
                }
                else if(x.Logs.Count()==0)
                {
                    return View("VarErr", x);
                }

                return View(x);
            }
            else
            {
                obj.SrouceList=new VarianceReportMangment(_Context).GetAllSources();
                return View("CalVar", obj);
            }
        }
        [HttpGet]
        public IActionResult VarCircuit()
        {
            VarReportOverCircuitModel viewModel = new VarReportOverCircuitModel()
            {
                Sources=new VarianceReportMangment(_Context).GetAllSources(),
            };
            return View(viewModel);
        }
        [HttpGet]
        public JsonResult GetMeasurments()
        {

            var Data = new VarianceReportMangment(_Context).GetAllMeasuremnt();
            return Json(new SelectList(Data, "Id", "Name"));
        }
        [HttpPost]
        public IActionResult CalVarOverCir(VarReportOverCircuitModel obj)
        {
            if(ModelState.IsValid)
            {
                var x = new VarianceReportMangment(_Context).CalculateVarOverCircuit(obj);

                return View(x);
            }
            else
            {
                obj.Sources=new VarianceReportMangment(_Context).GetAllSources();
                return View("VarCircuit", obj);
            }
        }
    }
}