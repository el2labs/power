﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Domain;
using E2LabPower.Mangment;
using E2LabPower.Data;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class LocationController : Controller
    {
        private DataContext _context;

        public LocationController(DataContext context)
        {
            _context=context;
        }
        public IActionResult Index()
        {
            var Locations = new LocationMangement(_context).GetAllLocations();
            if(Locations.Count()==0)
            {
                ModelState.AddModelError("", "There Is no Created Locations");
                return View(Locations);
            }
            return View(Locations);
        }
        [HttpGet]
        public IActionResult CreateLocation()
        {

            return View();
        }
        [HttpPost]
        public IActionResult CreateNewLocation(LocationDataModel obj)
        {
            if(ModelState.IsValid)
            {
                bool Created = new LocationMangement(_context).CreateNewLocation(obj);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("CreateLocation", obj);
            }
        }
        public IActionResult DeleteLocation(int Id)
        {
            bool Modified = new LocationMangement(_context).ModifyCircuits(Id);
            if(Modified)
            {
                bool deleted = new LocationMangement(_context).DeleteLocation(Id);
                if(deleted)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    //add Error View 
                    return View();
                }

            }else
            {
                return View();
            }
        }
        [HttpGet]
        public IActionResult EidtLocation(int Id)
        {
            var LocationData = new LocationMangement(_context).GetLocationByID(Id);
            return View(LocationData);
        }
        [HttpPost]
        public IActionResult EidtLocation(LocationDataModel obj)
        {
            if(ModelState.IsValid)
            {
                bool Updated = new LocationMangement(_context).EditLocation(obj);
                if(Updated)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View("EidtLocation", obj);
            }
        }

    }
}