﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Data;
using E2LabPower.Mangment.ReportsMangments;
using E2LabPower.Domain.EnergyWithPro;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class EnergyWithProController : Controller
    {
        private readonly DataContext _Context;
        public EnergyWithProController(DataContext context)
        {
            _Context=context;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult CalculateEnergyForCircuit()
        {
            EnergyWithProDataModel ViewModel = new EnergyWithProDataModel()
            {
                Sources=new VarianceReportMangment(_Context).GetAllSources()
            };
            return View(ViewModel);
        }
        [HttpPost]
        public IActionResult GetEnergyForCircuitPro(EnergyWithProDataModel obj)
        {
            var x = new EnergyWithPro(_Context).GetFullData(obj);
            
            return View(x);
        }
    }
}