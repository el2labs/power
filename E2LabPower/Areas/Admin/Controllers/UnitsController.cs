﻿using E2LabPower.Data;
using E2LabPower.Mangment;
using E2LabPower.Models;
using Microsoft.AspNetCore.Mvc;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UnitsController : Controller
    {
        private readonly DataContext _context;

        public UnitsController(DataContext context)
        {
            _context=context;
        }



        public IActionResult Index()
        {
            var Model = new UnitsMangements(_context).GetAllUnits();
            return View(Model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(UnitOfMeasurements obj)
        {
            if(ModelState.IsValid)
            {
                bool Exsit = new UnitsMangements(_context).CheckTheUnit(obj.Name);
                if(!Exsit)
                {
                    bool Created = new UnitsMangements(_context).AddUnit(obj);
                    if(Created)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("", "this Unit Is already Exsit");
                    return View(obj);

                }
            }
            else
            {
                return View(obj);
            }
        }

        //[HttpPost]
     
        public IActionResult Delete(int Id )
        {
            bool UnitNotAssgined = new UnitsMangements(_context).CheckUnitInProduct(Id);
            if(UnitNotAssgined)
            {
                bool Deleted = new UnitsMangements(_context).DeleteUnit(Id);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ModelState.AddModelError("", "You Can Not Deleted This Unit Because it's already Assigned to Product");
                return View(Id);
            }
        }
    }
}