﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Domain;
using E2LabPower.Data;
using E2LabPower.Mangment;

namespace E2LabPower.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductController : Controller
    {
        private readonly DataContext _Context;

        public ProductController(DataContext Data)
        {
            _Context=Data;
        }
        public IActionResult Index()
        {
            var Model = new ProductMangement(_Context).GetAllProduct();
            return View(Model);
        }
        [HttpGet]
        public IActionResult Create()
        {
            UnitsMangements unitmangement = new UnitsMangements(_Context);
            ProductDataModel viewModel = new ProductDataModel()
            {
                Units=unitmangement.GetAllUnits()
            };
            return View(viewModel);
        }
        [HttpPost]
        public IActionResult CreateProduct(ProductDataModel obj)
        {
            UnitsMangements unitmangement = new UnitsMangements(_Context);
            ProductDataModel viewModel = new ProductDataModel()
            {
                Name=obj.Name,
                UnitId=obj.UnitId,
                Id=obj.Id,
                Discretion=obj.Discretion,
                MeasuremntUnit=obj.MeasuremntUnit,
                Units=unitmangement.GetAllUnits()
            };

            if(ModelState.IsValid)
            {
                bool Created = new ProductMangement(_Context).CreateProduct(obj);
                if(Created)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View("Create", viewModel);
            }
        }
        public IActionResult Delete(int Id)
        {
            bool Deleted = new ProductMangement(_Context).Delete(Id);
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public IActionResult Edit(int Id)
        {
            ProductDataModel viewModel = new ProductDataModel();

            viewModel=new ProductMangement(_Context).GetProductByID(Id);
            if(viewModel==null)
            {
                return NotFound();
            }
            else
            {
                return View(viewModel);
            }
        }
        [HttpPost]
        public IActionResult Edit(ProductDataModel obj)
        {
            if(ModelState.IsValid)
            {
                bool Updated = new ProductMangement(_Context).Edit(obj);
                if(Updated)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(obj);
                }
            }
            else
            {
                return View("Edit", obj);
            }
        }

    }
}