﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Models
{
    [Table("production")]
    public class Production
    {
        [Key]
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public DateTime EndTime { get; set; }
        public Decimal Quantity { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public decimal ProductionEnergy { get; set; }
        public Location Location { get; set; }
        public int LocationID { get; set; }
    }
}
