﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Models
{
    [Table("logs_tbl")]
    public class LogModel
    {
        [Key]
        public int LogID { get; set; }
        public int NodeID { get; set; }
        public Decimal MeasurementValue { get; set; }
        public DateTime LogTimeStamp { get; set; }
        public int MeasurementID { get; set; }

    }
}
