﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Models
{
    [Table("nodes_tbl")]
    public class NodesModel
    {
        [Key]
        public int NodeID { get; set; }

        [Required]
        [Display(Name ="Line Name")]
        public string NodeName { get; set; }  
        
        public int SourceID { get; set; }

        [Required]
        [Display(Name = "Line Code")]
        public string LineCode { get; set; }

        public IEnumerable<MeasurmentChartModel> MeasurmentCharts { get; set; }
    }
}
