﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace E2LabPower.Models
{
    [Table("measurmenttype")]
    public class MeasuermentTypes
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Unit { get; set; }
    }
}
