﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Data;
using E2LabPower.Mangment;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace E2LabPower.Controllers
{
    public class DashBoardController : Controller
    {
        private DataContext _context;
        public IMemoryCache MemoryCache { get; }
        public IConfiguration Configuration { get; }

        public DashBoardController(DataContext con, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _context=con;
            MemoryCache=memoryCache;
            Configuration=configuration;
        }
        [HttpGet]
        public IActionResult Index()
        {
            SourceService sourceService = new SourceService(_context, MemoryCache, Configuration);
            var Sources = sourceService.GetAll();
            ViewData["SourceList"]=new SelectList(Sources, "Id", "Name");
            return View();
        }
        [HttpPost]
        public IActionResult Index(int Id)
        {
            SourceService sourceService = new SourceService(_context, MemoryCache, Configuration);
            var Sources = sourceService.GetAll();
            ViewData["SourceList"]=new SelectList(Sources, "Id", "Name");
            DashBoradService boradService = new DashBoradService(_context);
            var Lines = boradService.GetLinesWithLastValue(Id);
            return View(Lines);
        }
    }
}