﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Primitives;

using E2LabPower.Domain;
using System.IO;
using Microsoft.Extensions.Caching.Memory;
using E2LabPower.Data;
using OfficeOpenXml;
using System.Text;
using E2LabPower.Models;
using Microsoft.EntityFrameworkCore;

namespace E2LabPower.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class UpdatesController : Controller
    {
        private IConfiguration _Configuration;
        public DataContext Context { get; }
        private IMemoryCache _MemoryCache { get; }

        public UpdatesController(DataContext context, IMemoryCache MemoryCache, IConfiguration Configuration)
        {
            _Configuration=Configuration;
            Context=context;
            this._MemoryCache=MemoryCache;
        }
        // GET: api/Updates
        [HttpGet]
        public async Task<string> update()
        {
            var queryString = HttpContext.Request.Query;
            var values = queryString.Where(o => o.Key.StartsWith("p")).ToDictionary(o => o.Key);
            KeyValuePair<string, StringValues> deviceID;
            if(values.TryGetValue("pid", out deviceID))
            {
                //lessening  on Node  coed 
                var line = Context.Nodes.Single(n => n.LineCode==deviceID.Value);
                var con = new MySqlConnection(_Configuration["ConnectionStrings:e2l_powerDb"]);
                var cmd = con.CreateCommand();
                cmd.CommandText="INSERT INTO `logs_tbl`(`MeasurementID`, `MeasurementValue`,`NodeID`, `LogTimeStamp`) VALUES (@MeasurementID, @MeasurementValue,@NodeID, @LogTimeStamp)";
                cmd.Parameters.Add("MeasurementID", MySqlDbType.Int32);
                cmd.Parameters.AddWithValue("NodeID", line.NodeID);
                cmd.Parameters.Add("MeasurementValue", MySqlDbType.Decimal);
                cmd.Parameters.AddWithValue("LogTimeStamp", DateTime.Now);
                await con.OpenAsync();
                int MeasurementID;
                UpdateProcessing(values);
                foreach(var reading in values)
                {
                    if(int.TryParse(reading.Key.Substring(1), out MeasurementID))
                    {
                        cmd.Parameters["MeasurementID"].Value=MeasurementID;
                        cmd.Parameters["MeasurementValue"].Value=decimal.Parse(reading.Value.Value);
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                return $"Ok {deviceID.Value}";
            }
            else
            {
                return "worng format or missing Line Code";
            }
        }
        private void UpdateProcessing(Dictionary<string, KeyValuePair<string, StringValues>> values)
        {
            //calculate power req : p = I * V * P
            KeyValuePair<string, StringValues> Amper, Volt ,phase;
            if(values.TryGetValue("p1", out Volt)&&values.TryGetValue("p2", out Amper)&&values.TryGetValue("p3",out phase))
            {
                decimal AmperValue, VoltValue , phaseValue;
                decimal.TryParse(Amper.Value, out AmperValue);
                decimal.TryParse(Volt.Value, out VoltValue);
                decimal.TryParse(phase.Value, out phaseValue);
                var PowerValue = AmperValue*VoltValue*phaseValue;
                values.Add("p5", new KeyValuePair<string, StringValues>("p5", PowerValue.ToString()));
            }
        }
        public async Task<FileStreamResult> Export(int MID, int NodeID)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            MemoryStream fileMemoryStream = new MemoryStream();
            using(ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Export");
             //   var measuermentValues = await new LineService(Context, _MemoryCache, _Configuration).GetMeasurmentEntryByMeasurmentID(MID, NodeID);

                var Values = Context.logModel.Where(l => l.NodeID==NodeID&&l.MeasurementID==MID ).AsNoTracking().AsEnumerable();

                worksheet.Cells[1, 1].Value="Time";
                worksheet.Cells[1, 2].Value="Value";
                int i = 2;
                foreach(var row in Values)
                {
                    worksheet.Cells[i, 1].Value=row.LogTimeStamp;
                    worksheet.Cells[i, 1].Style.Numberformat.Format="dd/MM/yyy hh:mm:ss AM/PM";
                    worksheet.Cells[i, 2].Value=row.MeasurementValue;
                    i++;
                }
                worksheet.Column(1).Width=22;
                package.SaveAs(fileMemoryStream);
            }
            fileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(
          fileMemoryStream,
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          "report.xlsx");
        }
        public List<LogModel> GetListToExport(int NodeID, int MID, DateTime sd, DateTime ed)
        {
            List<LogModel> ValuesList = null;
            var values = Context.logModel.Where(l => l.NodeID==NodeID&&l.MeasurementID==MID&&l.LogTimeStamp>=sd&&l.LogTimeStamp<=ed).ToList();
            if(values.Count()!=0)
            {
                ValuesList=new List<LogModel>();
                foreach(var row in values)
                {
                    ValuesList.Add(new LogModel() {
                        LogTimeStamp = row.LogTimeStamp,
                        MeasurementValue = row.MeasurementValue

                    });
                }
            }
            return ValuesList;
        }

        public async Task<FileStreamResult> ExportWithEF(List<LogModel> Values)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            MemoryStream fileMemoryStream = new MemoryStream();
            using(ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Export");
                worksheet.Cells[1, 1].Value="Time";
                worksheet.Cells[1, 2].Value="Value";
                int i = 2;
                foreach(var row in Values)
                {
                    worksheet.Cells[i, 1].Value=row.LogTimeStamp;
                    worksheet.Cells[i, 1].Style.Numberformat.Format="dd/MM/yyy hh:mm:ss AM/PM";
                    worksheet.Cells[i, 2].Value=row.MeasurementValue;
                    i++;
                }
                worksheet.Column(1).Width=22;
                package.SaveAs(fileMemoryStream);
            }
            fileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(
          fileMemoryStream,
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          "report.xlsx");
        }

    }
}
