﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using E2LabPower.Mangment;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace E2LabPower.Controllers
{
    public class MeasurementsByTimeDurationController : Controller
    {
        private DataContext _context;
        private IMemoryCache memoryCache;
        private IConfiguration configuration;

        public MeasurementsByTimeDurationController(DataContext con, IMemoryCache MemoryCache, IConfiguration Configuration)
        {
            _context= con;
            configuration=Configuration;
            memoryCache=MemoryCache;
        }


        public IActionResult Index()
        {
            var model = _context.Sources.ToList();
            ViewData["SourceList"]=new SelectList(model, "Id", "Name");
            return View();
        }
        [HttpPost]
        public IActionResult Index(int Id , DateTime StartDate , DateTime EndDate)
        {
            var model = _context.Sources.ToList();
            ViewData["SourceList"]=new SelectList(model, "Id", "Name");
            var sourceSrv = new SourceService(_context, memoryCache, configuration);



            var lins = sourceSrv.GetLinesDataOfLast24hForSourceStartDate(Id, StartDate, EndDate);
            return View(lins);
        }
    }
}