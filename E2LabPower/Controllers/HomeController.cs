﻿using E2LabPower.Data;
using E2LabPower.Domain;
using E2LabPower.Domain.Model;
using E2LabPower.Mangment;
using E2LabPower.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
namespace E2LabPower.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMemoryCache memoryCache;
        private readonly IConfiguration configuration;
        private readonly DataContext _context;
      

        public HomeController(IMemoryCache memoryCache, IConfiguration configuration,DataContext cn)
        {
            this.memoryCache = memoryCache;
            this.configuration = configuration;
            _context = cn;
            
        }
  
        public IActionResult Index()
        {
            var model = _context.Sources.ToList();
             ViewData["SourceList"] = new SelectList(model, "Id", "Name");
            return View();
        }
 
        [HttpPost]
        public IActionResult Index(int Id,int DateTime)
        {     
            var model = _context.Sources.ToList();
            ViewData["SourceList"] = new SelectList(model, "Id", "Name");
            var sourceSrv = new SourceService(_context,memoryCache,configuration);
            var lins = sourceSrv.GetLinesDataOfLast24hForSource(Id,DateTime);
            return View(lins);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
