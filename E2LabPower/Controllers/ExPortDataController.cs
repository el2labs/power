﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E2LabPower.Data;
using E2LabPower.Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using E2LabPower.Mangment.ReportsMangments;
using E2LabPower.Domain.Variance_Report;
using E2LabPower.Models;
using System.Text;
using System.IO;
using OfficeOpenXml;
using E2LabPower.Mangment;
using Microsoft.EntityFrameworkCore;

namespace E2LabPower.Controllers
{
    public class ExPortDataController : Controller
    {
        private DataContext _Context;
        private IMemoryCache memoryCache;
        private IConfiguration configuration;

        public ExPortDataController(DataContext context)
        {
            _Context=context;
        }
        public JsonResult GetLines2(int SourceID)
        {
            var Data = new LineService(_Context, memoryCache, configuration).GetLinesBySourceID(SourceID).ToList();
            return Json(new SelectList(Data, "LineID", "LineName"));
        }
        public JsonResult GetLineMeasurments2(int LineID)
        {

            var Data = new VarianceReportMangment(_Context).GetMeasurmentList(LineID);
            return Json(new SelectList(Data, "Id", "Name"));
        }
        [HttpGet]
        public IActionResult ExportDataReport()
        {
            VarianceReportDataModel viewmodel = new VarianceReportDataModel()
            {
                SrouceList=new VarianceReportMangment(_Context).GetAllSources(),
            };

            return View(viewmodel);
        }
        [HttpPost]
        public IActionResult ExportFullData(VarianceReportDataModel obj)
        {
            obj.LineName=new LineMangement(_Context).getLineNameByID(obj.LineID);
            obj.MeasureName=new MeasurementServive(_Context).getM_NameByID(obj.MID);
         
            return View(obj);
        }
        public FileStreamResult ExportWithEF(int lineID ,int MID ,DateTime sd , DateTime ED)
        {

            List<LogModel> ValuesList = null;
            var values = _Context.logModel.Where(l => l.NodeID==lineID&& l.MeasurementID==MID && l.LogTimeStamp>=sd && l.LogTimeStamp<=ED).AsNoTracking().AsEnumerable();
            if(values.Any())
            {
                ValuesList=new List<LogModel>();
                foreach(var row in values)
                {
                    ValuesList.Add(new LogModel()
                    {
                        LogTimeStamp=row.LogTimeStamp,
                        MeasurementValue=row.MeasurementValue

                    });
                }
            }



            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            MemoryStream fileMemoryStream = new MemoryStream();
            using(ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Export");
                worksheet.Cells[1, 1].Value="Time";
                worksheet.Cells[1, 2].Value="Value";
                int i = 2;
                foreach(var row in ValuesList)
                {
                    worksheet.Cells[i, 1].Value=row.LogTimeStamp;
                    worksheet.Cells[i, 1].Style.Numberformat.Format="dd/MM/yyy hh:mm:ss AM/PM";
                    worksheet.Cells[i, 2].Value=row.MeasurementValue;
                    i++;
                }
                worksheet.Column(1).Width=22;
                package.SaveAs(fileMemoryStream);
            }
            fileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(
          fileMemoryStream,
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          "report.xlsx");
        }
    }
}