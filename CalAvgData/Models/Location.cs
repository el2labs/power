﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Models
{
    [Table("location")]
    public class Location
    {
        [Key]
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }
}
