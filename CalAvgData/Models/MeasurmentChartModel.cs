﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Models
{
    [Table("measurements_tbl")]
    public class MeasurmentChartModel
    {
        [Key]
        public int MeasurementID { get; set; }
        public int NodeID { get; set; }
        public string MeasurementName { get; set; }
        public decimal LineLastValue { get; set; }
        public virtual List<LogModel> LogEntry { get; set; }


    }
}
