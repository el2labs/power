﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Models
{
    public class MeasermentChartID
    {
        public int MeasurementID { get; set; }
        public int NodeID { get; set; }
        public string MeasurementName { get; set; }
        public List<LogModel> LogEntry { get; set; }
        public int ContainerID { get; set; }
    }
}
