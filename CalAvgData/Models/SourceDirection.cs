﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalculateAvgValues.Models
{
    [Table("source_Direction")]
    public class SourceDirection
    {
        [Key]
        public int SourceDirectionID { get; set; }
        public string Name { get; set; }
    }
}
