﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateAvgValues.DataModels
{
    public class CircuitsWithFullAVGDataModel
    {
        public int CircuitID { get; set; }
        public string CircuitName { get; set; }
        public List<LinesDataModel> Lines { get; set; }

    }
}
