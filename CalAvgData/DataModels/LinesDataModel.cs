﻿using System.Collections.Generic;

namespace CalculateAvgValues.DataModels
{
    public class LinesDataModel
    {
        public int LineID { get; set; }
        public string LineName { get; set; }
        public List<MeasurmentsDataModel> AssginedMeasurments { get; set; }
       
    }
}