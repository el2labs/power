﻿using System.Collections.Generic;

namespace CalculateAvgValues.DataModels
{
    public class MeasurmentsDataModel
    {
        public MeasurmentsDataModel()
        {
            LogsData=new List<Logs>();
        }


        public int MID { get; set; }
        public string MName { get; set; }
        public List<Logs> LogsData { get; set; }
        public decimal AvgValue { get; set; }
    }
}